/**
 * @fileoverview Redux Store
 */

import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";

import reducers from "src/redux/reducers";

// initialize dev tools in chrome
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// create redux store
export default createStore(
  reducers,
  composeEnhancers(applyMiddleware(reduxThunk))
);
