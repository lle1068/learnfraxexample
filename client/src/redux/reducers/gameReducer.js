import {
  SET_PLAY_GAME,
  SET_GAME_LOADED,
  SET_FULLSCREEN,
} from 'src/redux/actions/types';

const INITIAL_STATE = {
  isPlaying: false,
  isLoaded: false,
  isFullscreen: false,
};

const gameReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_PLAY_GAME: {
      return { ...state, isPlaying: action.payload };
    }
    case SET_GAME_LOADED: {
      return { ...state, isLoaded: action.payload };
    }
    case SET_FULLSCREEN: {
      return { ...state, isFullscreen: action.payload };
    }
    default:
      return state;
  }
};

export default gameReducer;
