/**
 * @fileoverview combines all reducers
 */

import { combineReducers } from "redux";

import gameReducer from "src/redux/reducers/gameReducer";

export default combineReducers({
  game: gameReducer,
});
