import {
  SET_PLAY_GAME,
  SET_GAME_LOADED,
  SET_FULLSCREEN,
} from 'src/redux/actions/types';

export const setPlayGame = (play) => ({ type: SET_PLAY_GAME, payload: play });

export const setGameLoaded = (loaded) => ({
  type: SET_GAME_LOADED,
  payload: loaded,
});

export const setFullscreen = (fullscreen) => ({
  type: SET_FULLSCREEN,
  payload: fullscreen,
});
