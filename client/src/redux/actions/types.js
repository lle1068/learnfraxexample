// Game
export const SET_PLAY_GAME = 'SET_PLAY_GAME';
export const SET_GAME_LOADED = 'SET_GAME_LOADED';
export const SET_FULLSCREEN = 'SET_FULLSCREEN';
