import React, { useState, useEffect, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import ErrorBoundary from 'src/components/Error/ErrorBoundary';
import Header from 'src/components/Header';
import MobileMenu from 'src/components/Navigation/MobileMenu';
import Home from 'src/components/Home'; // Note: not lazy loaded because used as landing component for routing tests
import { LazyAbout, LazyError404 } from 'src/components/LazyLoad';
import SignUpForm from 'src/components/SignUpForm';

const App = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  const isPlaying = useSelector((state) => state.game.isPlaying);

  useEffect(() => {
    if (isPlaying) {
      setShowMobileMenu(false);
    }
  }, [isPlaying]);

  return (
    <main>
      <Header
        showMobileMenu={showMobileMenu}
        setShowMobileMenu={setShowMobileMenu}
      />
      <MobileMenu
        showMobileMenu={showMobileMenu}
        setShowMobileMenu={setShowMobileMenu}
      />
      <ErrorBoundary>
        <Suspense fallback={null}>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/about' component={LazyAbout} />
            <Route exact path='/signup' component={SignUpForm} />
            <Route component={LazyError404} />
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </main>
  );
};

export default App;
