import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import BackgroundImage from 'src/components/BackgroundCoverImage';
import Play from 'src/components/Play';

import { containerWithHeader } from 'src/lib/lib-css-mixins';

const StyledHomeContainer = styled.div`
  ${containerWithHeader}
`;

const Home = () => (
  <StyledHomeContainer>
    <Helmet>
      <title>Home | Learn Fractions!</title>
      <meta name='description' content='Home | Learn Fractions' />
    </Helmet>
    <Play />
    <BackgroundImage src='assets/img/element5-digital-OyCl7Y4y0Bk-unsplash.jpg' />
  </StyledHomeContainer>
);

export default Home;
