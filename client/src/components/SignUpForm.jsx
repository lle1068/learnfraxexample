import React, { useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import Button from 'src/components/Buttons/Button';
import Input from 'src/components/Input';

const StyledSignUpFormWrapper = styled.div`
  min-height: ${(props) => `calc(100vh - ${props.theme.config.headerHeight})`};
  align-items: center;
  display: flex;
  margin: auto;
  margin-top: 12rem;
  flex-direction: column;
  max-width: 123rem;
`;

const StyledSignUpForm = styled.form`
  width: 100%;
  max-width: 400px;
  background-color: #fff;
  border: 1px solid gray;

  .signup-form-header {
    height: 3rem;
    margin-top: 2rem;
    display: flex;
    justify-content: center;

    h1 {
      font-size: 3rem;
      font-weight: 500;
      color: ${(props) => `${props.theme.colors.inputLabelTextColor}`};
      text-align: center;
    }
  }

  .signup-form-container {
    display: flex;
    flex-direction: column;
    margin: 0 5rem;
  }

  .submitButton {
    margin: 2rem auto 2rem 0;
    padding: 1.5rem 3rem;
  }
`;

// const initialState = {
//   username: '',
//   email: '',
//   password: ''
// }

const SignUpForm = () => {
  // const [state, setState] = useState(initialState);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // const [error, setError] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('Submitted!', e);
  };

  const inputData = [
    {
      id: 'username',
      type: 'text',
      name: 'username',
      label: 'Username',
      value: username,
      onChange: (e) => setUsername(e.target.value),
    },
    {
      id: 'email',
      type: 'email',
      name: 'email',
      label: 'Email',
      value: email,
      onChange: (e) => setEmail(e.target.value),
    },
    {
      id: 'password',
      type: 'password',
      name: 'password',
      label: 'Password',
      value: password,
      onChange: (e) => setPassword(e.target.value),
    },
  ];

  // const handleInput = e => {

  // }

  return (
    <StyledSignUpFormWrapper>
      <Helmet>
        <title>Sign Up | Learn Fractions!</title>
        <meta name='description' content='Sign Up | Learn Fractions' />
      </Helmet>
      <StyledSignUpForm onSubmit={handleSubmit}>
        <div className='signup-form-header'>
          <h1>LET'S GET STARTED</h1>
        </div>
        <div className='signup-form-container'>
          {inputData.map((data) => (
            <Input
              key={data.id}
              type={data.type}
              name={data.name}
              label={data.label}
              value={data.value}
              onChange={data.onChange}
            />
          ))}
          <Button className='submitButton' type='submit'>
            Submit
          </Button>
        </div>
      </StyledSignUpForm>
    </StyledSignUpFormWrapper>
  );
};

export default SignUpForm;
