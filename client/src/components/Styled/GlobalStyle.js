/**
 * @fileoverview Global css style
 */

import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  /* prevent default browser styling to be added to elements */
  *,
  *:after,
  *:before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }
  /* Allows easy conversion of px to REM */
  /* Normally 1 rem is 16px, but now 1 rem is 10px */
  html {
    font-size: 62.5%;
    font-family: 'Trebuchet MS';
  }

  /* Settings for inheritence */
  body {
    box-sizing: border-box; /* prevents margin and padding from being added to total width or height */
  }  
`;

export default GlobalStyle;
