/**
 * @fileoverview Define global theme and variables for styled components
 */

export const maxSmallMobile = 480;
export const maxMobile = 568;
export const maxTablet = 768;

const globalTheme = {
  colors: {
    black: '#000',
    white: '#fff',
    aqua: '#7FDBFF',
    link: '#0074D9',
    blueButton: '#4eb5f1',
    startGameButton: '#2ECC40',
    headerBackground: 'rgba(0,0,0,0.9)',
    mobileMenuButton: '#f0f0f0',
    disabled: 'hsla(0, 0%, 100%, 0.3)',
    inputLabelTextColor: '#5b5b5b',
    inputBackgroundColor: '#f5f5f5',
    inputBorderColor: '#5b5b5b',
  },
  breakpoints: {
    maxSmallMobile: `${maxSmallMobile}px`,
    maxMobile: `${maxMobile}px`,
    maxTablet: `${maxTablet}px`,
  },
  config: {
    headerHeight: '10.5rem',
    mobileMenuHeight: '27rem',
    top: '1000',
  },
};

export default globalTheme;
