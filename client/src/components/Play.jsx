import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { setPlayGame } from 'src/redux/actions';
import Button from 'src/components/Buttons/Button';
import Game, { initGame, endGame } from 'src/Game';

import { containerWithHeader } from 'src/lib/lib-css-mixins';

const StyledPlayContainer = styled.div`
  ${containerWithHeader};
  height: ${(props) => props.isFullscreen && '100vh'};

  display: flex;
  align-items: center;
  justify-content: center;

  .start-game-button {
    position: absolute;
  }
`;

const StyledStartButton = styled(Button)`
  opacity: ${(props) => (props.clickedStartButton ? 0 : 1)};
  background-color: ${(props) =>
    props.clickedStartButton
      ? props.theme.colors.black
      : props.theme.colors.startGameButton};
  transition: opacity 1s ease-in, background-color 0.2s ease;
`;

const Play = () => {
  const isPlaying = useSelector((state) => state.game.isPlaying);
  const isFullscreen = useSelector((state) => state.game.isFullscreen);
  const dispatch = useDispatch();
  const history = useHistory();

  const [startTransition, setStartTransition] = useState(false);
  const [clickedStartButton, setClickedStartButton] = useState(false);

  // Notifications regarding browser navigation during play
  useEffect(() => {
    const unblock = history.block((location, action) => {
      if (isPlaying) {
        const answer = window.confirm(
          `WARNING: Unsaved game progress will be lost!\nDo you want to navigate back to ${window.location.origin}${location.pathname}?`
        );
        if (answer === true) {
          dispatch(setPlayGame(false));
        }
        return answer;
      }
      return true; // Do not block
    });

    if (isPlaying) {
      window.onbeforeunload = () => true; // Confirmation on browser reloading and address bar navigation
    }

    return function cleanup() {
      window.onbeforeunload = null;
      unblock();
    };
  }, [isPlaying, dispatch, history]);

  useEffect(() => {
    if (isPlaying) {
      initGame();

      // screen lock hints for browsers
      if (window.ScreenOrientation?.lock) {
        ScreenOrientation.lock('landscape');
      } else if (window.screen.lockOrientation) {
        window.screen.lockOrientation('landscape');
      }
    } else {
      endGame();
      setStartTransition(false);
      setClickedStartButton(false);
      dispatch(setPlayGame(false));

      // screen unlock hints for browsers
      if (window.ScreenOrientation?.unlock) {
        ScreenOrientation.unlock();
      } else if (window.screen.unlockOrientation) {
        window.screen.unlockOrientation();
      }
    }
  }, [isPlaying, dispatch]);

  const handleClick = () => {
    if (!isPlaying) {
      setClickedStartButton(true);
      setStartTransition(true);
    }
  };

  return (
    <StyledPlayContainer isFullscreen={isFullscreen}>
      <Game
        startTransition={startTransition}
        startGame={() => {
          if (startTransition) {
            dispatch(setPlayGame(true));
          }
        }}
      />
      {!isPlaying && (
        <StyledStartButton
          className='start-game-button'
          onClick={handleClick}
          clickedStartButton={clickedStartButton}
          fontSize='2rem'
        >
          Start Game
        </StyledStartButton>
      )}
    </StyledPlayContainer>
  );
};

export default Play;
