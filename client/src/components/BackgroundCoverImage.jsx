import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledBackgroundCoverImage = styled.div`
  position: absolute;
  height: 100vh;
  width: 100%;
  top: 0;
  z-index: -1; // makes it act sort of like a background
  background-repeat: no-repeat;
  background-size: cover;
  background-position: left 50% bottom 20%;
  background-image: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)),
    url(${(props) => props.src});
`;

const BackgroundCoverImage = (props) => {
  BackgroundCoverImage.propsTypes = {
    url: PropTypes.string,
  };

  const { src } = props;

  return (
    <StyledBackgroundCoverImage src={src} data-testid='background-cover-img' />
  );
};

export default BackgroundCoverImage;
