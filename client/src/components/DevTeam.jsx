import React from 'react';
import styled from 'styled-components';

import ProfileCard from 'src/components/ProfileCard';

const StyledDevTeam = styled.article`
  margin: 0 4rem;

  .devteam-header {
    height: 12rem;
    display: flex;
    align-items: center;
    justify-content: center;

    h1 {
      font-size: 4rem;
      font-weight: 100;
      color: #5b5b5b;
      text-align: center;
    }

    border-bottom: 1px solid lightgrey;
  }

  .devteam-container {
    margin: 8rem 0;
    display: flex;
    align-items: center;
    justify-content: space-evenly;

    @media (max-width: ${(props) => props.theme.breakpoints.maxTablet}) {
      margin: 3rem auto;
      flex-direction: column;

      & > * {
        margin: 3rem auto;
      }
    }
  }
`;

const DevTeam = () => (
  <StyledDevTeam>
    <div className='devteam-header'>
      <h1>Development Team</h1>
    </div>
    <div className='devteam-container'>
      <ProfileCard
        imgSrc='assets/img/david.jpg'
        name='David Coopersmith'
        jobTitle='Fullstack Developer & Math Teacher'
        location='Moreno Valley, CA'
        email='mailto:example@gmail.com'
      />
      <ProfileCard
        imgSrc='assets/img/long.jpg'
        name='Long Le'
        jobTitle='Software Engineer & Web Developer'
        location='Eugene, OR'
        email='mailto:example@gmail.com'
      />
    </div>
  </StyledDevTeam>
);

export default DevTeam;
