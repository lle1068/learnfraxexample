/**
 * @fileoverview code split components
 */

import { lazy } from 'react';

export const LazyAbout = lazy(() => import('src/components/About'));
export const LazyError404 = lazy(() => import('src/components/Error/Error404'));
export const LazyError500 = lazy(() => import('src/components/Error/Error500'));
