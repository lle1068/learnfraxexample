import React from 'react';

import { render, act } from 'src/util/test-utils';
import Play from 'src/components/Play';

describe('Test Play', () => {
  it('Should render', () => {
    let wrapped = null;

    act(() => {
      wrapped = render(<Play />);
    });

    wrapped.getByText('Start Game');
  });
});
