import React from 'react';

import { render, act } from 'src/util/test-utils';
import Home from 'src/components/Home';

describe('Test Home', () => {
  it('Should render', () => {
    let wrapped = null;

    act(() => {
      wrapped = render(<Home />);
    });

    wrapped.getByText('Start Game');
    wrapped.getByTestId('background-cover-img');
  });
});
