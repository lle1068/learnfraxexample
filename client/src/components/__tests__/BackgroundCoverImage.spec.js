import React from 'react';

import { render, act } from 'src/util/test-utils';
import BackgroundCoverImage from 'src/components/BackgroundCoverImage';

describe('Test BackgroundCoverImage', () => {
  it('Should render', () => {
    let wrapped = null;

    act(() => {
      wrapped = render(<BackgroundCoverImage />);
    });

    wrapped.getByTestId('background-cover-img');
  });
});
