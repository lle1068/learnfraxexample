import React from 'react';

import { render, act, cleanup } from 'src/util/test-utils';
import About from 'src/components/About';

describe('Test About', () => {
  let wrapped = null;

  beforeEach(() => {
    act(() => {
      wrapped = render(<About />);
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render header', () => {
    wrapped.getByText('Development Team');
  });

  it('Should render David', () => {
    wrapped.getByText('David Coopersmith');
    wrapped.getByText('Fullstack Developer & Math Teacher');
    wrapped.getByText('Moreno Valley, CA');
  });

  it('Should render Long', () => {
    wrapped.getByText('Long Le');
    wrapped.getByText('Software Engineer & Web Developer');
    wrapped.getByText('Eugene, OR');
  });

  it('Should render two Contact links', () => {
    const contacts = wrapped.getAllByText('Contact');
    expect(contacts.length).toBe(2);
  });
});
