import React from 'react';

import { render, act } from 'src/util/test-utils';
import ProfileCard from 'src/components/ProfileCard';

describe('Test ProfileCard', () => {
  it('Should render', () => {
    let wrapped = null;
    const name = 'john smith';
    const jobTitle = 'pen tester';
    const location = 'tokyo, japan';
    const email = 'test@email.com';

    act(() => {
      wrapped = render(
        <ProfileCard
          name={name}
          jobTitle={jobTitle}
          location={location}
          email={email}
        />
      );
    });

    wrapped.getByText(name);
    wrapped.getByText(jobTitle);
    wrapped.getByText(location);
    const link = wrapped.getByText('Contact');

    expect(link).toHaveAttribute('href');
  });
});
