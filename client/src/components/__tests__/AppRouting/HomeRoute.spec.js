import React from 'react';

import { render, act, cleanup } from 'src/util/test-utils';
import App from 'src/components/App';

describe('Test Home Route', () => {
  let wrapped = null;

  beforeEach(() => {
    act(() => {
      wrapped = render(<App />, {}, { route: '/' });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render home route', async () => {
    wrapped.getByText('Learn Fractions!');
    const abouts = wrapped.getAllByText('About');
    expect(abouts.length).toBe(2);
    const logins = wrapped.getAllByText('Log In');
    expect(logins.length).toBe(2);
    const signups = wrapped.getAllByText('Sign Up');
    expect(signups.length).toBe(2);
    wrapped.getByText('Start Game');
    wrapped.getByTestId('background-cover-img');
  });
});
