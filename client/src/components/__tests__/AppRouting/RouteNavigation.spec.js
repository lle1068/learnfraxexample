import React from 'react';

import { render, act, cleanup, userEvent } from 'src/util/test-utils';
import App from 'src/components/App';

describe('Test Route Navigation', () => {
  let wrapped = null;

  beforeEach(() => {
    act(() => {
      wrapped = render(<App />, {}, { route: '/' });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render home route', async () => {
    wrapped.getByText('Learn Fractions!');
    const abouts = wrapped.getAllByText('About');
    expect(abouts.length).toBe(2);
    const logins = wrapped.getAllByText('Log In');
    expect(logins.length).toBe(2);
    const signups = wrapped.getAllByText('Sign Up');
    expect(signups.length).toBe(2);
    wrapped.getByText('Start Game');
    wrapped.getByTestId('background-cover-img');
  });

  it('Should render about route after clicking', async () => {
    const startGame = wrapped.getByText('Start Game');
    const backgroundImg = wrapped.getByTestId('background-cover-img');
    const abouts = wrapped.getAllByText('About');

    act(() => {
      userEvent.click(abouts[1]);
    });

    expect(startGame).not.toBeInTheDocument();
    expect(backgroundImg).not.toBeInTheDocument();

    await wrapped.findByText('Development Team');
    await wrapped.findByText('David Coopersmith');
    await wrapped.findByText('Fullstack Developer & Math Teacher');
    await wrapped.findByText('Moreno Valley, CA');
    await wrapped.findByText('Long Le');
    await wrapped.findByText('Software Engineer & Web Developer');
    await wrapped.findByText('Eugene, OR');
    const contacts = await wrapped.findAllByText('Contact');
    expect(contacts.length).toBe(2);
  });
});
