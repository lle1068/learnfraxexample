import React from 'react';

import { render, act, cleanup } from 'src/util/test-utils';
import App from 'src/components/App';

describe('Test 404', () => {
  let wrapped = null;

  beforeEach(() => {
    act(() => {
      wrapped = render(<App />, {}, { route: '/invalid' });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render home route', async () => {
    await wrapped.findByText('Learn Fractions!');
    await wrapped.findAllByText('About');
    await wrapped.findAllByText('Log In');
    await wrapped.findAllByText('Sign Up');
    await wrapped.findByText('Error 404');
    await wrapped.findByText('Sorry, page was not found!');
  });
});
