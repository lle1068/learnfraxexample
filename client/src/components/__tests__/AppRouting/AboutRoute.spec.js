import React from 'react';

import { render, act, cleanup } from 'src/util/test-utils';
import App from 'src/components/App';

describe('Test About Route', () => {
  let wrapped = null;

  beforeEach(() => {
    act(() => {
      wrapped = render(<App />, {}, { route: '/about' });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render about route', async () => {
    await wrapped.findByText('Learn Fractions!');
    await wrapped.findAllByText('About');
    await wrapped.findAllByText('Log In');
    await wrapped.findAllByText('Sign Up');
    await wrapped.findByText('Development Team');
    await wrapped.findByText('David Coopersmith');
    await wrapped.findByText('Fullstack Developer & Math Teacher');
    await wrapped.findByText('Moreno Valley, CA');
    await wrapped.findByText('Long Le');
    await wrapped.findByText('Software Engineer & Web Developer');
    await wrapped.findByText('Eugene, OR');
    const contacts = await wrapped.findAllByText('Contact');
    expect(contacts.length).toBe(2);
  });
});
