import React from 'react';

import { render, act } from 'src/util/test-utils';
import FlipCard from 'src/components/FlipCard';

describe('Test FlipCard', () => {
  it('Should render', () => {
    let wrapped = null;
    const name = 'john smith';
    const jobTitle = 'pen tester';
    const location = 'tokyo, japan';
    const description = 'fake person';
    const note = 'fake note';

    act(() => {
      wrapped = render(
        <FlipCard
          name={name}
          jobTitle={jobTitle}
          location={location}
          description={description}
          note={note}
        />
      );
    });

    wrapped.getByText(name);
    wrapped.getByText(jobTitle);
    wrapped.getByText(location);
    wrapped.getByText(description);
    wrapped.getByText(note);
  });
});
