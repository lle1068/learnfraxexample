import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import NavItems from 'src/components/Navigation/NavItems';

const StyledNavBar = styled.nav`
  display: flex;
  flex-direction: row;
  margin: 0 5rem;
`;

const NavBar = (props) => {
  NavBar.propTypes = {
    className: PropTypes.string,
  };

  const { className } = props;

  return (
    <StyledNavBar className={className}>
      <NavItems />;
    </StyledNavBar>
  );
};

export default NavBar;
