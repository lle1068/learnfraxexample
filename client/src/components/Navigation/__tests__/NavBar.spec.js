import React from 'react';

import { render, act } from 'src/util/test-utils';
import NavBar from 'src/components/Navigation/NavBar';

describe('Test NavBar', () => {
  it('Should render', () => {
    let wrapped = null;
    act(() => {
      wrapped = render(<NavBar />);
    });
    wrapped.getByText('About');
  });
});
