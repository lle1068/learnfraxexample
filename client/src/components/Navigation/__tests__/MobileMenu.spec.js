import React from 'react';

import { render, act } from 'src/util/test-utils';
import MobileMenu from 'src/components/Navigation/MobileMenu';

describe('Test MobileMenu', () => {
  it('Should render', () => {
    let wrapped = null;
    act(() => {
      wrapped = render(<MobileMenu />);
    });
    wrapped.getByText('Home');
    wrapped.getByText('About');
    wrapped.getByText('Sign Up');
    wrapped.getByText('Log In');
  });
});
