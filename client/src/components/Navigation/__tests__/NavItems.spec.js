import React from 'react';

import { render, act, cleanup, userEvent } from 'src/util/test-utils';
import NavItems from 'src/components/Navigation/NavItems';

describe('Test NavItems - Desktop', () => {
  let wrapped = null;
  const handleClick = jest.fn();

  beforeEach(() => {
    act(() => {
      wrapped = render(<NavItems closeMobileMenu={handleClick} />);
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render', () => {
    wrapped.getByText('About');
  });

  it('Should click', () => {
    const link = wrapped.getByText('About');

    act(() => {
      userEvent.click(link);
    });

    expect(handleClick).toBeCalledTimes(1);
  });
});

describe('Test NavItems - Mobile', () => {
  let wrapped = null;
  const handleClick = jest.fn();

  beforeEach(() => {
    act(() => {
      wrapped = render(<NavItems closeMobileMenu={handleClick} mobile />);
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render', () => {
    wrapped.getByText('Home');
    wrapped.getByText('About');
    wrapped.getByText('Sign Up');
    wrapped.getByText('Log In');
  });

  it('Should click Home', () => {
    const link = wrapped.getByText('Home');

    act(() => {
      userEvent.click(link);
    });

    expect(handleClick).toBeCalledTimes(1);
  });

  it('Should click About', () => {
    const link = wrapped.getByText('About');

    act(() => {
      userEvent.click(link);
    });

    expect(handleClick).toBeCalledTimes(1);
  });

  it('Should click Sign Up', () => {
    const link = wrapped.getByText('Sign Up');

    act(() => {
      userEvent.click(link);
    });

    expect(handleClick).toBeCalledTimes(1);
  });

  it('Should click Log In', () => {
    const link = wrapped.getByText('Log In');

    act(() => {
      userEvent.click(link);
    });

    expect(handleClick).toBeCalledTimes(1);
  });
});
