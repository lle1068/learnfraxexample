import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import NavItems from 'src/components/Navigation/NavItems';

const StyledMobileMenu = styled.nav`
  position: absolute;
  height: ${(props) =>
    props.showMobileMenu ? props.theme.config.mobileMenuHeight : '0'};
  width: 100%;
  background-color: ${(props) => props.theme.colors.headerBackground};
  display: flex;
  align-items: center;
  justify-content: flex-start;
  overflow: hidden;
  z-index: ${(props) => props.theme.config.top};

  transition: height 0.5s ease;
`;

const StyledMenuItemContainer = styled.div`
  margin: 2rem 4rem;
  display: flex;
  flex-direction: column;
  width: 100%;

  & > * {
    margin: 1.5rem 0;
    padding: 0;
    font-size: 2.5rem;
    color: lightgrey;
    overflow: hidden;
  }
`;

const MobileMenu = (props) => {
  MobileMenu.propTypes = {
    showMobileMenu: PropTypes.bool,
    setShowMobileMenu: PropTypes.func,
  };

  const { showMobileMenu = false, setShowMobileMenu = () => null } = props;

  return (
    <StyledMobileMenu showMobileMenu={showMobileMenu}>
      <StyledMenuItemContainer>
        <NavItems mobile closeMobileMenu={() => setShowMobileMenu(false)} />
      </StyledMenuItemContainer>
    </StyledMobileMenu>
  );
};

export default MobileMenu;
