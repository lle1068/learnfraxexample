import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import { disabledLink } from 'src/lib/lib-css-mixins';

const StyledNavItem = styled(NavLink)`
  color: hsla(0, 0%, 100%, 0.7);
  font-size: 1.5rem;
  text-align: center;
  margin: 0 1rem;
  text-decoration: none;

  &:hover {
    color: white;
    text-decoration: underline;
  }

  ${disabledLink}

  transition: color 0.5s ease-in-out;
`;

const NavItems = (props) => {
  NavItems.propTypes = {
    mobile: PropTypes.bool,
    closeMobileMenu: PropTypes.func,
  };

  const { mobile = false, closeMobileMenu = () => null } = props;

  const isPlaying = useSelector((state) => state.game.isPlaying);

  const handleClick = (e) => {
    if (isPlaying) {
      e.preventDefault();
    }

    closeMobileMenu();
  };

  return (
    <>
      {mobile && (
        <StyledNavItem to='/' onClick={handleClick} disabled={isPlaying}>
          Home
        </StyledNavItem>
      )}
      <StyledNavItem to='/about' onClick={handleClick} disabled={isPlaying}>
        About
      </StyledNavItem>
      {mobile && (
        <>
          <StyledNavItem to='' onClick={handleClick} disabled={isPlaying}>
            Sign Up
          </StyledNavItem>
          <StyledNavItem to='' onClick={handleClick} disabled={isPlaying}>
            Log In
          </StyledNavItem>
        </>
      )}
    </>
  );
};

export default NavItems;
