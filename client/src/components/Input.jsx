import React from 'react';
import styled from 'styled-components';

const StyledLabel = styled.label`
  font-size: 1.8rem;
  color: ${(props) => `${props.theme.colors.inputLabelTextColor}`};
  margin-top: 3rem;
`;

const StyledInput = styled.input`
  font-size: 1.8rem;
  margin: 0.5rem 0;
  padding: 12px 10px;
  border-width: 0.5px;
  border-style: solid;
  border-color: ${(props) => `${props.theme.colors.inputBorderColor}`};
  border-radius: 5px;
  background-color: ${(props) => `${props.theme.colors.inputBackgroundColor}`};
`;

const Input = (props) => {
  const { name, label, id, ...rest } = props;

  return (
    <>
      <StyledLabel htmlFor={id}>{label}</StyledLabel>
      <StyledInput {...rest} name={name} id={id} />
    </>
  );
};

export default Input;
