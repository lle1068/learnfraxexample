import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledFlipCard = styled.div`
  /* 
  * The flip card container - set the width and height to whatever you want.
  * We have added the border property to demonstrate that the flip itself goes out of the box on hover
  * (remove perspective if you don't want the 3D effect)
  */
  .flip-card {
    background-color: transparent;
    width: 300px;
    height: 300px;
    perspective: 1000px; /* Remove this if you don't want the 3D effect */
  }

  /* This container is needed to position the front and back side */
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.8s;
    transform-style: preserve-3d;
  }

  /* Do an horizontal flip when you move the mouse over the flip box container */
  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  /* Position the front and back side */
  .flip-card-front,
  .flip-card-back {
    position: absolute;
    border-radius: 10px;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden; /* Safari */
    backface-visibility: hidden;
  }

  /* Style the front side (fallback if image is missing) */
  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  /* Style the back side */
  .flip-card-back {
    background-color: ${(props) =>
      props.backColor || props.theme.colors.blueButton};
    color: white;
    transform: rotateY(180deg);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    & > * {
      margin: 0.9rem auto;
    }

    .avatar-name {
      font-size: 2.5rem;
    }

    .avatar-job-title {
      font-size: 1.5rem;
      font-style: italic;
    }

    .avatar-location {
      font-size: 1.5rem;
    }
  }
`;

const FlipCard = (props) => {
  FlipCard.propTypes = {
    imgSrc: PropTypes.string,
    alt: PropTypes.string,
    imgHeight: PropTypes.string,
    imgWidth: PropTypes.string,
    name: PropTypes.string,
    jobTitle: PropTypes.string,
    location: PropTypes.string,
    description: PropTypes.string,
    note: PropTypes.string,
    backColor: PropTypes.string,
  };

  const {
    imgSrc,
    imgAlt = 'avatar',
    imgHeight = '30rem',
    imgWidth = '30rem',
    name,
    jobTitle,
    location,
    description,
    note,
    backColor,
  } = props;

  const imgStyle = {
    height: imgHeight,
    width: imgWidth,
    borderRadius: '10px',
  };

  return (
    <StyledFlipCard backColor={backColor}>
      <div className='flip-card'>
        <div className='flip-card-inner'>
          <div className='flip-card-front'>
            <img src={imgSrc} alt={imgAlt} style={imgStyle} />
          </div>
          <div className='flip-card-back'>
            {name && <h2 className='avatar-name'>{name}</h2>}
            {jobTitle && <p className='avatar-job-title'>{jobTitle}</p>}
            {location && <p className='avatar-location'>{location}</p>}
            {description && <p className='avatar-description'>{description}</p>}
            {note && <p className='avatar-note'>{note}</p>}
          </div>
        </div>
      </div>
    </StyledFlipCard>
  );
};

export default FlipCard;
