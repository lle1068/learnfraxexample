/**
 * @fileoverview ErrorBoundary for catching network errors
 */

import React, { Suspense } from 'react';

import { LazyError500 } from 'src/components/LazyLoad';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // i.e logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return (
        <Suspense fallback={null}>
          <LazyError500 />
        </Suspense>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
