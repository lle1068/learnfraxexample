import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import { containerWithHeader } from 'src/lib/lib-css-mixins';

const StyledError404 = styled.article`
  ${containerWithHeader}
  margin: 0 auto;
  max-width: 115rem;
  text-align: center;
  font-family: 'system-ui';
  display: flex;
  align-items: center;
  justify-content: center;

  .error-title {
    margin: 3rem;
    font-size: 8rem;
    font-weight: 200;
  }

  .error-description {
    margin: 3rem;
    font-size: 2rem;
    font-weight: 200;
  }
`;

const Error404 = () => (
  <StyledError404>
    <Helmet>
      <title>Error 404</title>
      <meta name='description' content='Error 404' />
    </Helmet>
    <div>
      <h1 className='error-title'>Error 404</h1>
      <h2 className='error-description'>Sorry, page was not found!</h2>
    </div>
  </StyledError404>
);

export default Error404;
