import React from 'react';

import { render, act } from 'src/util/test-utils';
import Error404 from 'src/components/Error/Error404';

describe('Test Error404', () => {
  it('Should render', () => {
    let wrapped = null;

    act(() => {
      wrapped = render(<Error404 />);
    });

    wrapped.getByText('Error 404');
    wrapped.getByText('Sorry, page was not found!');
  });
});
