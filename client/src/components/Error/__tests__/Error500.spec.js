import React from 'react';

import { render, act } from 'src/util/test-utils';
import Error500 from 'src/components/Error/Error500';

describe('Test Error500', () => {
  it('Should render', () => {
    let wrapped = null;

    act(() => {
      wrapped = render(<Error500 />);
    });

    wrapped.getByText('Error 500');
    wrapped.getByText(
      'Sorry, something has gone wrong. Please try again later.'
    );
  });
});
