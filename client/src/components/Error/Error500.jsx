import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import { containerWithHeader } from 'src/lib/lib-css-mixins';

const StyledError500 = styled.article`
  ${containerWithHeader}
  margin: 0 auto;
  max-width: 115rem;
  text-align: center;
  font-family: 'system-ui';
  display: flex;
  align-items: center;
  justify-content: center;

  .error-title {
    margin: 3rem;
    font-size: 8rem;
    font-weight: 200;
  }

  .error-description {
    margin: 3rem;
    font-size: 2rem;
    font-weight: 200;
  }
`;

const Error500 = () => (
  <StyledError500>
    <Helmet>
      <title>Error 500 | Internal Server Error</title>
      <meta name='description' content='Error 500' />
    </Helmet>
    <div>
      <h1 className='error-title'>Error 500</h1>
      <h2 className='error-description'>
        Sorry, something has gone wrong. Please try again later.
      </h2>
    </div>
  </StyledError500>
);

export default Error500;
