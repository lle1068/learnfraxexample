import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledButton = styled.button`
  background-color: ${(props) =>
    props.backgroundColor || props.theme.colors.blueButton || 'transparent'};
  color: ${(props) => props.color || props.theme.colors.white};
  font-size: ${(props) => props.fontSize};
  border: none;
  margin: auto;
  padding: 0.4em 1.2em;
  border-radius: 0.5rem;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  text-align: center;
  transition: all 0.2s ease;

  &:hover {
    cursor: pointer;
    ${(props) => props.invert && `color: ${props.theme.colors.black}`};
    background-color: ${(props) =>
      props.invert ? props.theme.colors.white : 'rgba(0, 0, 0, 0.85)'};
  }

  @media (max-width: 30em) {
    margin: 0.2em auto;
  }
`;

const Button = (props) => {
  Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    fontSize: PropTypes.string,
    invert: PropTypes.bool,
  };

  const {
    children,
    className,
    onClick,
    backgroundColor,
    color,
    fontSize = '1.5rem',
    invert,
  } = props;

  return (
    <StyledButton
      className={className}
      onClick={onClick}
      backgroundColor={backgroundColor}
      color={color}
      fontSize={fontSize}
      invert={invert}
    >
      {children}
    </StyledButton>
  );
};

export default Button;
