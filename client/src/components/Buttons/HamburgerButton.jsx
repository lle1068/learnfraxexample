/**
 * @fileoverview animated Hamburger menu button
 */

import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const StyledHamburgerButton = styled.button`
  margin: 0;
  padding: 0;
  border: none;
  background-color: transparent;
  transform: scale(0.75);

  ${(props) =>
    props.activateMediaBreakpoints &&
    css`
      @media (min-width: ${(props) => props.theme.breakpoints.maxTablet}) {
        display: none;
      }
    `}

  .hamburger-button {
    background: none;
    border: none;
    width: 60px;
    height: 45px;
    position: relative;
    margin: 0;
    transform: rotate(0deg);
    transition: 0.5s ease-in-out;
    cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};
  }

  .hamburger-button span {
    display: block;
    position: absolute;
    height: 9px;
    width: 100%;
    background-color: ${(props) =>
      props.disabled
        ? props.theme.colors.disabled
        : props.backgroundColor || props.theme.colors.mobileMenuButton};
    border-radius: 3px;
    opacity: 1;
    left: 0;
    transform: rotate(0deg);
    transition: 0.25s ease-in-out;
  }

  .hamburger-button span:nth-child(1) {
    top: 0px;
  }

  .hamburger-button span:nth-child(2) {
    top: 18px;
  }

  .hamburger-button span:nth-child(3) {
    top: 36px;
  }

  .hamburger-button.open span:nth-child(1) {
    top: 18px;
    transform: rotate(135deg);
  }

  .hamburger-button.open span:nth-child(2) {
    opacity: 0;
    left: -60px;
  }

  .hamburger-button.open span:nth-child(3) {
    top: 18px;
    transform: rotate(-135deg);
  }
`;

const HamburgerButton = (props) => {
  HamburgerButton.propTypes = {
    activateMediaBreakpoints: PropTypes.bool,
    backgroundColor: PropTypes.string,
    activateMenu: PropTypes.func,
    showMobileMenu: PropTypes.bool,
  };

  const {
    activateMediaBreakpoints,
    backgroundColor,
    activateMenu = () => null,
    showMobileMenu,
  } = props;

  const isPlaying = useSelector((state) => state.game.isPlaying);

  return (
    <StyledHamburgerButton
      activateMediaBreakpoints={activateMediaBreakpoints}
      backgroundColor={backgroundColor}
      disabled={isPlaying}
      data-testid='hamburger-button-wrapper'
    >
      <div
        className={`hamburger-button ${showMobileMenu ? 'open' : ''}`}
        onClick={isPlaying ? null : activateMenu}
        data-testid='hamburger-button'
      >
        <span />
        <span />
        <span />
      </div>
    </StyledHamburgerButton>
  );
};

export default HamburgerButton;
