import React from 'react';

import { render, act, cleanup, userEvent } from 'src/util/test-utils';
import HamburgerButton from 'src/components/Buttons/HamburgerButton';

describe('Test HamburgerButton Not During Gameplay', () => {
  let wrapped = null;
  const handleClick = jest.fn();

  beforeEach(() => {
    act(() => {
      wrapped = render(<HamburgerButton activateMenu={handleClick} />, {
        initialState: {
          game: {
            isPlaying: false,
          },
        },
      });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render', () => {
    wrapped.getByTestId('hamburger-button');
  });

  it('Should click', () => {
    const hamburgerButton = wrapped.getByTestId('hamburger-button');

    act(() => {
      userEvent.click(hamburgerButton);
    });

    expect(handleClick).toBeCalledTimes(1);
  });
});

describe('Test HamburgerButton During Gameplay', () => {
  let wrapped = null;
  const handleClick = jest.fn();

  beforeEach(() => {
    act(() => {
      wrapped = render(<HamburgerButton activateMenu={handleClick} />, {
        initialState: {
          game: {
            isPlaying: true,
          },
        },
      });
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render disabled', () => {
    const hamburgerButton = wrapped.getByTestId('hamburger-button-wrapper');
    expect(hamburgerButton).toBeDisabled();
  });

  it('Should click', () => {
    const hamburgerButton = wrapped.getByTestId('hamburger-button');

    act(() => {
      userEvent.click(hamburgerButton);
    });

    expect(handleClick).toBeCalledTimes(0);
  });
});
