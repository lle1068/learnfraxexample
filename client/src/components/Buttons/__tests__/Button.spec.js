import React from 'react';

import { render, act, cleanup, userEvent } from 'src/util/test-utils';
import Button from 'src/components/Buttons/Button';

describe('Test Button', () => {
  let wrapped = null;
  const handleClick = jest.fn();

  beforeEach(() => {
    act(() => {
      wrapped = render(<Button onClick={handleClick}>button</Button>);
    });
  });

  afterEach(() => {
    cleanup();
  });

  it('Should render', () => {
    wrapped.getByText('button');
  });

  it('Should click', () => {
    const button = wrapped.getByText('button');

    act(() => {
      userEvent.click(button);
    });

    expect(handleClick).toBeCalledTimes(1);
  });
});
