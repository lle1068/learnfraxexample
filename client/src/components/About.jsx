import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import DevTeam from 'src/components/DevTeam';

const StyledAbout = styled.div`
  margin: 0 auto;
  max-width: 123rem;
`;

const About = () => (
  <StyledAbout>
    <Helmet>
      <title>About | Learn Fractions!</title>
      <meta name='description' content='About | Learn Fractions' />
    </Helmet>
    <DevTeam />
  </StyledAbout>
);

export default About;
