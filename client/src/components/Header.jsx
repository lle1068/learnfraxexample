import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import NavBar from 'src/components/Navigation/NavBar';
import Button from 'src/components/Buttons/Button';
import HamburgerButton from 'src/components/Buttons/HamburgerButton';

const StyledHeader = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  height: ${(props) => props.theme.config.headerHeight};
  background-color: ${(props) => {
    if (props.isPlaying) {
      return 'rgba(0,0,0,1)';
    }

    return props.backgroundColor || props.theme.colors.headerBackground;
  }};
  border-bottom: 3px solid ${(props) => props.theme.colors.aqua};

  transition: background-color 0.8s ease-in-out;

  .header-container {
    display: flex;
    flex-direction: row;
    width: 115rem;
    justify-content: space-between;
    margin: 0 4rem;

    .header-title {
      color: ${(props) => props.theme.colors.aqua};
      font-weight: 500;
      font-size: 4rem;
      font-family: 'Candara';
      font-style: italic;

      &:hover {
        cursor: pointer;
      }

      @media (max-width: ${(props) => props.theme.breakpoints.maxSmallMobile}) {
        font-size: 2.5rem;
      }
    }
  }

  .header-left-group {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    .header-nav-bar {
      @media (max-width: ${(props) => props.theme.breakpoints.maxTablet}) {
        display: none;
      }
    }
  }

  .header-right-group {
    display: flex;
    align-items: center;
    justify-content: flex-end;

    .header-login-group {
      display: flex;
      flex-direction: row;
      align-items: center;

      @media (max-width: ${(props) => props.theme.breakpoints.maxTablet}) {
        display: none;
      }
    }
  }
`;

const StyledLoginButton = styled.button`
  font-size: 1.5rem;
  padding: 1rem;
  border: none;
  background-color: transparent;
  color: ${(props) => props.theme.colors.white};

  &:hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

const Header = (props) => {
  Header.propTypes = {
    backgroundColor: PropTypes.string,
    showMobileMenu: PropTypes.bool,
    setShowMobileMenu: PropTypes.func,
  };

  const history = useHistory();

  const isPlaying = useSelector((state) => state.game.isPlaying);

  const {
    backgroundColor,
    showMobileMenu,
    setShowMobileMenu = () => null,
  } = props;

  return (
    <StyledHeader backgroundColor={backgroundColor} isPlaying={isPlaying}>
      <div className='header-container'>
        <div className='header-left-group'>
          <h1 className='header-title' onClick={() => history.push('/')}>
            Learn Fractions!
          </h1>
          <NavBar className='header-nav-bar' />
        </div>
        <div className='header-right-group'>
          <div className='header-login-group'>
            <StyledLoginButton>Log In</StyledLoginButton>
            <Button isPlaying={isPlaying} invert>
              Sign Up
            </Button>
          </div>
          <HamburgerButton
            activateMediaBreakpoints
            activateMenu={() => setShowMobileMenu(!showMobileMenu)}
            showMobileMenu={showMobileMenu}
          />
        </div>
      </div>
    </StyledHeader>
  );
};

export default Header;
