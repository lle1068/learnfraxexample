import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledProfileCard = styled.section`
  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    width: 30rem;
    margin: auto;
    text-align: center;
    background-color: rgba(255, 255, 255, 0.8);
    font-family: arial;

    @media (max-width: 380px) {
      width: 20rem;
    }
  }

  .avatar-img-wrapper {
    width: 30rem;
    height: 30rem;
    background-color: lightgray;

    @media (max-width: 380px) {
      width: 20rem;
      height: 20rem;
    }
  }

  .avatar-img {
    width: 100%;
    height: 100%;
  }

  .profile-info {
    display: flex;
    flex-direction: column;
    margin: 2rem;

    & > * {
      margin: 1rem 0;
    }

    .name {
      font-size: 2.5rem;
    }

    .title {
      color: grey;
      font-size: 18px;
      font-size: 1.5rem;
      font-style: italic;
    }

    .location {
      font-size: 1.5rem;
    }
  }

  a {
    text-decoration: none;
    font-size: 22px;
  }

  a:hover {
    opacity: 0.7;
  }
`;

const StyledButton = styled.button`
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: black;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;

  &:hover {
    opacity: 0.7;
  }
`;

const ProfileCard = (props) => {
  ProfileCard.propTypes = {
    imgSrc: PropTypes.string,
    imgAlt: PropTypes.string,
    name: PropTypes.string,
    jobTitle: PropTypes.string,
    location: PropTypes.string,
    description: PropTypes.string,
    note: PropTypes.string,
  };

  const { imgSrc, imgAlt = '', name, jobTitle, location, email } = props;

  return (
    <StyledProfileCard>
      <div className='card'>
        <div className='avatar-img-wrapper'>
          <img src={imgSrc} className='avatar-img' alt={imgAlt} />
        </div>
        <div className='profile-info'>
          {name && <h1 className='name'>{name}</h1>}
          {jobTitle && <p className='title'>{jobTitle}</p>}
          {location && <p className='location'>{location}</p>}
        </div>
        <p>
          <StyledButton as='a' href={email}>
            Contact
          </StyledButton>
        </p>
      </div>
    </StyledProfileCard>
  );
};

export default ProfileCard;
