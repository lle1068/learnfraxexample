import Phaser from 'phaser';

import { KEY as LEARNFRAX_KEY } from 'src/Game/scenes/LearnFrax';
import { offsetFromAnchor, verifyType } from 'src/lib/lib-game-utils';
import { Types } from 'src/Game/config/constants';

class BaseGame extends Phaser.Scene {
  init(data, preloadAssets = () => {}) {
    this.#preventStart = data.preventStart ?? false;
    this.#preload = data.preload ?? false;
    this.#preloadAssets = data.preloadAssets ?? preloadAssets;
  }

  preload() {
    if (this.#preload) {
      this.#preloadAssets(this);
    }

    this.#learnFraxScene = this.scene.get(LEARNFRAX_KEY);
    this.#centerX = this.scale.width / 2;
    this.#centerY = this.scale.height / 2;
    this.#anchor = { x: this.#centerX, y: this.#centerY };
  }

  create(config) {
    if (config) {
      this.#createInstructionText(config.instructionText);
    }

    if (!this.#preventStart) {
      this.startGame();
    }
  }

  get anchor() {
    return this.#anchor;
  }

  get learnFraxScene() {
    return this.#learnFraxScene;
  }

  get instructionText() {
    return this.#instructionText;
  }

  get centerX() {
    return this.#centerX;
  }

  get centerY() {
    return this.#centerY;
  }

  get started() {
    return this.#started;
  }

  set started(started) {
    if (!verifyType(Types.Boolean, started)) {
      return;
    }

    this.#started = started;
  }

  get interacted() {
    return this.#interacted;
  }

  set interacted(interacted) {
    if (!verifyType(Types.Boolean, interacted)) {
      return;
    }

    this.#interacted = interacted;
  }

  get hintText() {
    return this.#hintText;
  }

  set hintText(hintText) {
    if (!verifyType(Types.String, hintText)) {
      return;
    }

    this.#hintText = hintText;
  }

  #started = false;
  #interacted = false;
  #preventStart = null;
  #preload = null;
  #preloadAssets = null;
  #anchor = null;
  #instructionText = null;
  #learnFraxScene = null;
  #centerX = null;
  #centerY = null;
  #hintText = null;

  startGame = () => {};

  setInstructionText = (text = '') => {
    this.#instructionText.setText(text).setVisible(true);
  };

  #createInstructionText = (config) => {
    const { offset, textStyle = {} } = config;

    const textPosition = offsetFromAnchor(
      this.anchor,
      offset ?? { x: 0, y: 0 }
    );

    this.#instructionText = this.add
      .text(textPosition.x, textPosition.y, '', textStyle)
      .setOrigin(0.5, 0)
      .setVisible(false);
  };
}

export default BaseGame;
