/**
 * @fileoverview Base scene for Slice type games
 */

import Theme from 'src/Game/singletons/Theme';
import BaseGame from 'src/Game/scenes/Base/BaseGame';
import SliceObject from 'src/Game/components/SliceObject';
import {
  randomInt,
  getRandomDivider,
  getNewNumber,
  initPinchZoom,
} from 'src/lib/lib-game-utils';
import {
  FONT_SIZE,
  FONT_FAMILY,
  TEXT_OFFSET_Y,
} from 'src/Game/scenes/LearnFrax';
import NumberLine from 'src/Game/components/NumberLine';
import MistakeHandling from 'src/Game/singletons/MistakeHandling';

const MAX_DIVIDER = 4;
const FALL_BOUNDARY = 200;
const VERTICAL_TOLERANCE = 30;
const HORIZONTAL_TOLERANCE = 30;
const PROGRESS_POINTS = 10;
const MIN_WIDTH = 400;
const MAX_WIDTH = 600;
const CURSOR = 'url(assets/cursor/knife.cur), pointer';

class SliceBase extends BaseGame {
  constructor(
    key,
    {
      sectionSrc = [],
      soundsSrc = [],
      maxDivider = MAX_DIVIDER,
      fallBoundary = FALL_BOUNDARY,
      verticalTolerance = VERTICAL_TOLERANCE,
      horizontalTolerance = HORIZONTAL_TOLERANCE,
      progressPoints = PROGRESS_POINTS,
      minWidth = MIN_WIDTH,
      maxWidth = MAX_WIDTH,
      cursor = CURSOR,
      instructText = 'Cut',
    }
  ) {
    super(key);

    this.#sectionSrc = sectionSrc;
    this.#soundsSrc = soundsSrc;
    this.#maxDivider = maxDivider;
    this.#fallBoundary = fallBoundary;
    this.#verticalTolerance = verticalTolerance;
    this.#horizontalTolerance = horizontalTolerance;
    this.#progressPoints = progressPoints;
    this.#minWidth = minWidth;
    this.#maxWidth = maxWidth;
    this.#cursor = cursor;
    this.#instructText = instructText;
  }

  init(data) {
    super.init(data, data.preloadAssets);
    this.#childSceneReset = data.resetGame ?? (() => null);
  }

  preload() {
    super.preload();
  }

  create() {
    this.resetCursor();
    this.#createGraphics();
    this.#createSounds();
    initPinchZoom(this);

    super.create({
      instructionText: {
        offset: { x: 0, y: TEXT_OFFSET_Y },
        textStyle: {
          fill: Theme.countdownTimer,
          fontSize: FONT_SIZE,
          fontFamily: FONT_FAMILY,
        },
      },
    });
  }

  update() {
    this.#pauseSound();
    this.#verifySuccess();
  }

  #sectionSrc = null;
  #soundsSrc = null;
  #currentDivider = null;
  #points = [];
  #textureIndex = null;
  #sectionsMap = {};
  #sounds = [];
  #currentSound = null;
  #falling = false;
  #numberLine = null;
  #maxDivider = null;
  #fallBoundary = null;
  #verticalTolerance = null;
  #horizontalTolerance = null;
  #progressPoints = null;
  #sliceObject = null;
  #childSceneReset = null;
  #minWidth = null;
  #maxWidth = null;
  #pinchZoom = false;
  #toast = null;
  #toastTimer = null;
  #cursor = null;
  #instructText = null;

  get textureIndex() {
    return this.#textureIndex;
  }

  get falling() {
    return this.#falling;
  }

  set falling(fall) {
    if (typeof fall !== 'boolean') {
      return console.log('Error: falling must be boolean');
    }

    this.#falling = fall;
  }

  get currentSound() {
    return this.#currentSound;
  }

  get points() {
    return this.#points;
  }

  get currentDivider() {
    return this.#currentDivider;
  }

  get sliceObject() {
    return this.#sliceObject;
  }

  set textureIndex(textureIndex) {
    if (
      typeof textureIndex === 'number' &&
      textureIndex >= 0 &&
      textureIndex < this.#sectionSrc.length
    ) {
      this.#textureIndex = textureIndex;
    } else {
      console.log('Error: texture index is not an index of texture sources');
    }
  }

  get pinchZoom() {
    return this.#pinchZoom;
  }

  set pinchZoom(zoom) {
    if (typeof zoom !== 'boolean') {
      return console.log('Error: pinchZoom must be boolean');
    }

    this.#pinchZoom = zoom;
  }

  get dotHints() {
    return this.#numberLine.dotHints;
  }

  get progressPoints() {
    return this.#progressPoints;
  }

  getHintPos = () => ({ x: this.centerX, y: this.centerY });

  resetCursor = () => {
    this.input.setDefaultCursor(this.#cursor);
  };

  showTryAgainToast = () => {
    this.showToast('Oops. Try again!');
  };

  showKeepTryingToast = () => {
    this.showToast('Keep trying!');
  };

  showHints = (divider, limit) => {
    this.#numberLine.showHints(divider, limit);
  };

  showToast = (toastText, color = 'orange', duration = 2000) => {
    if (this.#toast) {
      this.#toast.destroy();
      this.#toast = null;

      if (this.#toastTimer) {
        clearTimeout(this.#toastTimer);
        this.#toastTimer = null;
      }
    }

    const [toast, timer] = MistakeHandling.showToast(
      this,
      { x: this.instructionText.x, y: this.instructionText.y + 200 },
      toastText,
      color,
      duration
    );

    this.#toast = toast;
    this.#toastTimer = timer;
  };

  generateInstructionText = () => {
    const text = `${this.#instructText} 1/${this.#currentDivider}`;
    this.setInstructionText(text);
  };

  resetPoints = () => {
    this.#points = [];
  };

  startGame = () => {
    this.generateInstructionText();
    this.instructionText.setVisible(true);
    this.started = true;
  };

  restartGame = () => {
    this.resetCursor();
    this.#sectionsMap = {};
    this.#falling = false;
    this.#numberLine.destroy();
    this.#numberLine = null;
    this.interacted = false;
    this.#createSliceObject();
    this.#createNumberLine();
    this.pickSound();
    this.generateInstructionText();
    this.instructionText.setVisible(true);
    MistakeHandling.resetIndex(this.scene.key);
    this.#childSceneReset();
  };

  pickSound = () => {
    this.#currentSound = this.#sounds[randomInt(0, this.#sounds.length)];
  };

  stopSounds = () => this.#sounds.forEach((sound) => sound.stop());

  verifyCut = (checkVerticalTolerances = true, singleCut = false) => {
    if (!this.#points.length || !this.#sliceObject) {
      return -1;
    }

    const position = this.#sliceObject.position;
    const width = this.#sliceObject.scaledWidth;
    const height = this.#sliceObject.scaledHeight;

    const firstPointerDownPosition = this.#points[0];

    if (checkVerticalTolerances) {
      // check top position
      const topY = firstPointerDownPosition.y + height / 2;
      if (topY > position.y + this.#verticalTolerance) {
        return -1;
      }

      // check bottom position
      const lastPointerDownPosition = this.#points[this.#points.length - 1];
      const bottomY = lastPointerDownPosition.y + height / 2;

      if (bottomY < position.y + height - this.#verticalTolerance) {
        return -1;
      }
    }

    const sectionWidth = width / this.#currentDivider;
    const evenNumber = this.#currentDivider % 2 === 0;

    // find which section is being cut
    for (let i = 0; i < this.#currentDivider - 1; i++) {
      if (this.#sectionsMap[i]) {
        continue; // skip already cut sections
      }

      let breakpoint =
        this.#currentDivider === 2
          ? position.x
          : position.x +
            i * sectionWidth -
            (evenNumber ? sectionWidth : sectionWidth / 2);

      // this horizontal translation is necessary for denominators > 4
      // not entirely sure where the offset translation comes from but this adjusts it
      if (this.#currentDivider > 4) {
        if (this.#currentDivider <= 6) {
          breakpoint -= sectionWidth * 2 - sectionWidth;
        } else if (this.#currentDivider > 6) {
          breakpoint -= sectionWidth * 3 - sectionWidth;
        }
      }

      if (
        firstPointerDownPosition.x >= breakpoint - this.#horizontalTolerance &&
        firstPointerDownPosition.x <= breakpoint + this.#horizontalTolerance
      ) {
        // check all horizontal positions to see if they are within tolerance
        for (const point of this.#points) {
          if (
            point.x < breakpoint - this.#horizontalTolerance ||
            point.x > breakpoint + this.#horizontalTolerance
          ) {
            return -1;
          }
        }

        if (!singleCut) {
          this.#sectionsMap[i] = true; // mark the successful cut
        }

        return i;
      }
    }

    return -1;
  };

  #pauseSound = () => {
    if (
      Math.abs(this.input.activePointer.velocity.x) < 4 &&
      Math.abs(this.input.activePointer.velocity.y) < 4 &&
      this.#currentSound?.isPlaying
    ) {
      this.#currentSound.pause();
    }
  };

  #getSectionKeys = () => this.#sectionSrc.map((section) => section.key);

  #verifySuccess = () => {
    if (
      this.#sliceObject &&
      this.#sliceObject.getFallPosition() >
        this.scale.height + this.#fallBoundary
    ) {
      const mistakeIndex = MistakeHandling.getIndex(this.scene.key);

      const points = {
        0: this.#progressPoints,
        1: this.#progressPoints,
        2: this.#progressPoints / 2,
      };

      this.learnFraxScene.addProgress(points[mistakeIndex]);
    }
  };

  #createSounds = () => {
    this.#soundsSrc.forEach((src) => {
      const sound = this.sound.add(src.key, { volume: 1, loop: true });
      this.#sounds.push(sound);
    });
  };

  #createNumberLine = () => {
    if (this.#numberLine) {
      this.#numberLine.destroy();
      this.#numberLine = null;
    }

    this.#numberLine = new NumberLine({
      scene: this,
      position: {
        x: this.centerX,
        y: this.centerY + this.#sliceObject.scaledHeight / 2 + 60,
      },
      length: this.#sliceObject.scaledWidth,
      depth: -1,
    });
  };

  #createSliceObject = () => {
    if (this.#sliceObject) {
      this.#sliceObject.destroy();
      this.#sliceObject = null;
    }

    this.#currentDivider = getNewNumber(this.#currentDivider, () =>
      getRandomDivider(this.#maxDivider, this.#currentDivider)
    );

    this.#sliceObject = new SliceObject({
      scene: this,
      position: { x: this.centerX, y: this.centerY },
      textures: this.#getSectionKeys(),
      sections: this.#currentDivider,
      maxWidth: randomInt(this.#minWidth, this.#maxWidth + 1), // range for size of slice object
    });
  };

  #createGraphics = () => {
    this.#createSliceObject();
    this.#createNumberLine();
  };
}

export default SliceBase;
