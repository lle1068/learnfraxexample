import Phaser from 'phaser';

import BaseGame from 'src/Game/scenes/Base/BaseGame';
import { KEY as BACKGROUND_KEY } from 'src/Game/scenes/Background';
import {
  FONT_SIZE,
  FONT_FAMILY,
  TEXT_OFFSET_Y,
} from 'src/Game/scenes/LearnFrax';
import { randomInt } from 'src/lib/lib-game-utils';
import Theme from 'src/Game/singletons/Theme';

export const KEY = 'Asteroids';
const INSTRUCTION_TEXT = 'Load containers';
const PROGRESS_POINTS = 10;
const SHIP_WIDTH = 60;
const SHIP_MAX_SPEED = 280;
const SHIP_SPEED = 1 / 2;
const CONTAINER_LINE_COLOR = 0xffff00;
const CONTAINER_LINE_THICKNESS = 3;
const CONTAINER_FILLED_COLOR = '#3792cb';
const MIN_DENOMINATOR = 2;
const MAX_DENOMINATOR = 6;
const MAX_ASTEROID_SIZE = 140;

export const preloadAsteroids = (scene) => {
  scene.load.atlas(
    'flares',
    'assets/particles/flares.png',
    'assets/particles/flares.json'
  );
  scene.load.image(
    'stars-background',
    'assets/backgrounds/wp3493593-black-space-wallpaper-hd.jpeg'
  );
  scene.load.audio('capture', 'assets/sounds/bell/capture.mp3');
};

class Asteroids extends BaseGame {
  constructor() {
    super(KEY);
  }

  init(data) {
    super.init(data, preloadAsteroids);
  }

  preload() {
    super.preload();
  }

  create() {
    this.#containers = this.physics.add.group();

    this.#createGame();
    this.#addShipControls();

    super.create({
      instructionText: {
        offset: { x: 0, y: TEXT_OFFSET_Y },
        textStyle: {
          fill: Theme.countdownTimer,
          fontSize: FONT_SIZE,
          fontFamily: FONT_FAMILY,
        },
      },
    });
  }

  update(time, delta) {
    this.#moveContainers();
    this.#moveAsteroids();
    this.#particles.setPosition(this.#ship.x, this.#ship.y);

    this.physics.world.wrap(this.#ship);
    this.physics.world.wrap(this.#asteroids);

    this.#background.tilePositionX += 25 * (delta / 1000);
  }

  #ship = null;
  #containers = null;
  #denominator = null;
  #snakePath = [];
  #snakeSpacer = 30; // parameter that sets the spacing between sections
  #lines = [];
  #particles = null;
  #asteroids = [];
  #background = null;

  setInstructionText = () => {
    this.instructionText.setText(INSTRUCTION_TEXT).setVisible(true);

    this.tweens.add({
      targets: this.instructionText,
      alpha: { from: 1, to: 0.2 },
      delay: 1000,
      duration: 2000,
      ease: 'Linear',
    });
  };

  startGame = () => {
    this.setInstructionText();
    this.#addCollisionsForObjects();
    this.started = true;
  };

  restartGame = () => {
    this.destroy();
    this.instructionText.setAlpha(1).setVisible(false);
    this.#createGame();
    this.#addCollisionsForObjects();
  };

  destroy = () => {
    this.#ship.destroy();
    this.#ship = null;
    this.#containers.clear(true, true);
    this.#asteroids.forEach((asteroid) => {
      asteroid.text.destroy();
      asteroid.destroy();
    });
    this.#asteroids = [];
    this.#lines.forEach((line) => line.destroy());
    this.#lines = [];
    this.#snakePath = [];
  };

  #preventControls = () => {
    if (!this.started) {
      return true;
    }

    return false;
  };

  #createGame = () => {
    this.scene.get(BACKGROUND_KEY).setBackgroundColor(0x000000);
    this.learnFraxScene.setFullscreenButtonColor('white');
    this.#createParallaxBackground();
    this.#generateDenominator();
    this.#createShip();
    this.#createContainers();
    this.#initSnakePath();
    this.#createAsteroids();
  };

  #createParallaxBackground = () => {
    if (this.#background) {
      this.#background.destroy();
    }

    this.#background = this.add
      .tileSprite(0, 0, this.scale.width, this.scale.height, 'stars-background')
      .setAlpha(0.4)
      .setOrigin(0);
  };

  #generateDenominator = () => {
    this.#denominator = randomInt(MIN_DENOMINATOR, MAX_DENOMINATOR + 1);
  };

  #createShip = () => {
    // TODO: update into a real sprite
    this.#ship = this.add
      .ellipse(this.centerX, this.centerY, SHIP_WIDTH, SHIP_WIDTH * 0.8)
      .setStrokeStyle(2, 0xffffff)
      .setDepth(2);

    this.physics.add.existing(this.#ship);

    const particles = this.add.particles('flares');
    this.#particles = particles.createEmitter({
      frame: { frames: ['red', 'green', 'blue'], cycle: true },
      scale: { start: 0.15, end: 0 },
      blendMode: 'ADD',
    });
  };

  #fractionText = () => {
    const fractionMap = {
      2: 'half',
      3: 'third',
      4: 'fourth',
      5: 'fifth',
      6: 'sixth',
    };

    return fractionMap[this.#denominator];
  };

  #createContainers = () => {
    for (let i = 0; i <= this.#denominator; i++) {
      const container = this.add
        .text(this.#ship.x, this.#ship.y, `one-${this.#fractionText()}`, {
          color: 'white',
          padding: {
            x: 20,
            y: 20,
          },
          backgroundColor: 'grey',
          fontSize: '18px',
        })
        .setOrigin(0.5)
        .setDepth(1);

      if (i === 0) {
        // placeholder container
        container.setVisible(false);
        container.loaded = true; // for win game state
      } else if (i > 1) {
        const line = this.add.graphics().setAlpha(0.2);
        this.#lines.push(line);
      }

      this.#containers.add(container, i !== 0);
    }

    this.#containers.setAlpha(0.6);
  };

  #initSnakePath = () => {
    for (let i = 0; i <= this.#denominator * this.#snakeSpacer; i++) {
      this.#snakePath.push(new Phaser.Geom.Point(this.#ship.x, this.#ship.y));
    }
  };

  #createAsteroids = () => {
    const numberOfAsteroids = this.#denominator + randomInt(3, 6);

    for (let i = 0; i < numberOfAsteroids; i++) {
      const asteroid = this.add
        .circle(randomInt(0, this.scale.width), randomInt(0, this.scale.height))
        .setStrokeStyle(1, 0xffffff);

      asteroid.text = this.add
        .text(asteroid.x, asteroid.y, '', {
          color: 'white',
          fontSize: '18px',
        })
        .setOrigin(0.5);

      if (i < this.#denominator) {
        // create primary asteroids for filling containers
        asteroid.setRadius(this.#getAsteroidSize(this.#denominator));
        asteroid.denominator = this.#denominator;
        asteroid.text.setText(`1/${this.#denominator}`);
      } else {
        // create extra asteroids with incorrect fractions
        const denominator = randomInt(MIN_DENOMINATOR, MAX_DENOMINATOR + 1);
        asteroid.setRadius(this.#getAsteroidSize(denominator));
        asteroid.text.setText(`1/${denominator}`);
        asteroid.denominator = denominator;
      }

      this.physics.add.existing(asteroid);
      asteroid.body.setVelocity(
        randomInt(50, 101, true),
        randomInt(50, 101, true)
      );
      this.#asteroids.push(asteroid);
    }
  };

  #addCollisionsForObjects = () => {
    this.physics.add.collider(this.#ship, this.#asteroids);
    this.physics.add.collider(
      this.#asteroids,
      this.#containers,
      null,
      this.#handleAsteroidContainerCollision
    );
    this.physics.add.collider(this.#asteroids, this.#asteroids);
  };

  #handleAsteroidContainerCollision = (asteroid, container) => {
    if (
      asteroid.denominator === this.#denominator &&
      !container.loaded &&
      asteroid.visible
    ) {
      asteroid.text.destroy();
      asteroid.destroy();
      container.loaded = true;
      container.setBackgroundColor(CONTAINER_FILLED_COLOR);

      this.sound.play('capture');
      const isCargoLoaded = this.#isContainerFullyLoaded();

      if (isCargoLoaded) {
        this.learnFraxScene.addProgress(PROGRESS_POINTS);
      }
    }
  };

  #isContainerFullyLoaded = () => {
    for (const container of this.#containers.getChildren()) {
      if (!container.loaded) {
        return false;
      }
    }

    return true;
  };

  #moveAsteroids = () => {
    this.#asteroids.forEach((asteroid) => {
      asteroid.text.setPosition(asteroid.x, asteroid.y);
    });
  };

  #getAsteroidSize = (denominator) => {
    if (!denominator) {
      return randomInt(30, MAX_ASTEROID_SIZE);
    }

    return MAX_ASTEROID_SIZE / denominator;
  };

  #addShipControls = () => {
    this.input
      .on('pointerdown', (pointer) => {
        if (this.#preventControls()) {
          return;
        }

        this.#moveAndRotateShip(pointer, SHIP_MAX_SPEED);
      })
      .on('pointermove', (pointer) => {
        if (this.#preventControls()) {
          return;
        }

        if (pointer.isDown) {
          this.#moveAndRotateShip(pointer, SHIP_MAX_SPEED);
        }
      });
  };

  #moveAndRotateShip = (pointer, constantSpeed = null) => {
    const shipPosition = { x: this.#ship.x, y: this.#ship.y };
    const pointerPosition = { x: pointer.worldX, y: pointer.worldY };

    const dist = Phaser.Math.Distance.BetweenPoints(
      shipPosition,
      pointerPosition
    );

    this.physics.moveTo(
      this.#ship,
      pointer.worldX,
      pointer.worldY,
      constantSpeed || Phaser.Math.Clamp(SHIP_SPEED * dist, 0, SHIP_MAX_SPEED)
    );

    this.#ship.rotation = Phaser.Math.Angle.BetweenPoints(
      shipPosition,
      pointerPosition
    );
  };

  #moveContainers = () => {
    if (!this.started) {
      return;
    }

    const end = this.#snakePath.pop();
    end.setTo(this.#ship.x, this.#ship.y);
    this.#snakePath.unshift(end);

    const containers = this.#containers.getChildren();

    for (let i = 1; i <= this.#denominator; i++) {
      containers[i].x = this.#snakePath[i * this.#snakeSpacer].x;
      containers[i].y = this.#snakePath[i * this.#snakeSpacer].y;

      this.#drawContainerConnectionLines(containers, i);
    }
  };

  #drawContainerConnectionLines = (containers, i) => {
    if (i > 1) {
      const line = this.#lines[i - 2];
      line.clear().lineStyle(CONTAINER_LINE_THICKNESS, CONTAINER_LINE_COLOR);

      const containerA = containers[i];
      const containerB = containers[i - 1];
      const gapTolerance = Math.min(this.centerX, this.centerY);

      const xGap = Math.abs(containerA.x - containerB.x);
      const yGap = Math.abs(containerA.y - containerB.y);

      if (xGap < gapTolerance && yGap < gapTolerance) {
        line.lineBetween(
          containers[i].x,
          containers[i].y,
          containers[i - 1].x,
          containers[i - 1].y
        );
      }
    }
  };
}

export default Asteroids;
