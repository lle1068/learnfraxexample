/**
 * @fileoverview LearnFrax games management scene
 */

import Phaser from 'phaser';
import shuffle from 'lodash/shuffle';

import store from 'src/redux/store';
import { setGameLoaded, setFullscreen } from 'src/redux/actions';
import Theme from 'src/Game/singletons/Theme';
import { convertHexColorToRGB, isMobile } from 'src/lib/lib-game-utils';

import {
  KEY as SPRAY_BARS_KEY,
  preloadSprayBars,
} from 'src/Game/scenes/SprayBars';
import {
  KEY as SLICE_CRACKERS_KEY,
  preloadSliceCrackers,
} from 'src/Game/scenes/SliceCrackers';
import {
  KEY as SLICE_BARS_KEY,
  preloadSliceBars,
} from 'src/Game/scenes/SliceBars';
import { KEY as EAT_KEY, preloadEat } from 'src/Game/scenes/Eat';
import { KEY as DRAWING_TOOLS_KEY } from 'src/Game/scenes/DrawingTools';
import {
  KEY as BACKGROUND_KEY,
  preloadBackground,
} from 'src/Game/scenes/Background';
import {
  KEY as ASTEROIDS_KEY,
  preloadAsteroids,
} from 'src/Game/scenes/Asteroids';
import { drawLoadingBar } from 'src/lib/lib-game-graphics';
import CountdownTimer from 'src/Game/components/CountdownTimer';
import RadialProgressBar from 'src/Game/components/RadialProgressBar';
import TextButton from 'src/Game/components/Buttons/TextButton';
import Hint from 'src/Game/components/Hint';

const POINTS_GOAL = 100;
const DEFAULT_VOLUME_LEVEL = 1;
export const KEY = 'LearnFrax';
export const TEXT_OFFSET_Y = -400;
export const FONT_SIZE = '100px';
export const FONT_FAMILY = 'Verdana';
const START_TIMER_SECONDS = 3;
const HINT_TIMER_SECONDS = 10;

class LearnFrax extends Phaser.Scene {
  constructor() {
    super(KEY);
  }

  preload() {
    if (
      isMobile(this) &&
      this.scale.fullscreen.available &&
      !this.scale.isFullscreen
    ) {
      this.scale.startFullscreen();
      store.dispatch(setFullscreen(true));
    }

    // at most behaves as a hint for browsers to lock orientation to landscape
    this.scale.lockOrientation(Phaser.Scale.LANDSCAPE);

    if (isMobile(this)) {
      this.load.image('rotatePhone', 'assets/mobile/rotate-phone.png');
    }

    this.load.audio('ding', 'assets/sounds/bell/ding.mp3');
    this.#preloadGames();
    preloadBackground(this);

    drawLoadingBar({ scene: this });
  }

  create() {
    this.#controls = this.add.group();
    this.#hint = new Hint({
      scene: this,
      text: '',
      style: {
        fontFamily: 'Courier',
        color: 'black',
        fontSize: isMobile(this) ? '50px' : '30px',
        align: 'center',
      },
    });

    this.#createOrientationNotice();
    this.#createGraphics();

    this.scene.run(BACKGROUND_KEY);

    if (!isMobile(this)) {
      this.scene.run(DRAWING_TOOLS_KEY);
    }

    this.#startTimer();
    store.dispatch(setGameLoaded(true));

    const rgb = convertHexColorToRGB(Theme.background);
    this.cameras.main.fadeFrom(1000, rgb[0], rgb[1], rgb[2]);
  }

  update() {
    this.#radialProgressBar.draw();
  }

  #radialProgressBar = null;
  #fullscreenButton = null;
  #volumeButton = null;
  #games = [];
  #currentGame = null;
  #currentPoints = 0;
  #started = false;
  #currentVolume = DEFAULT_VOLUME_LEVEL;
  #cameraControlButton = null;
  #controls = null;
  #lockOrientationScreen = null;
  #countdownTimer = null;
  #screenlock = false;
  #hint = null;

  get currentGame() {
    return this.#currentGame;
  }

  addProgress = (points, callback = () => {}) => {
    this.#currentPoints = Phaser.Math.Clamp(
      this.#currentPoints + (points || 0),
      0,
      POINTS_GOAL
    );

    if (points) {
      this.sound.play('ding'); // play sound to indicate correct
    }

    this.#radialProgressBar.addProgress(points, () => {
      callback();
      this.#checkProgress();
    });
    this.launchNextGame();
  };

  launchNextGame = (initialize = false) => {
    // FOR RUNNING JUST ONE GAME - TESTING
    if (
      this.#games.length === 1 &&
      this.#currentGame &&
      this.scene.isActive(this.#currentGame)
    ) {
      const gameInstance = this.scene.get(this.#currentGame);
      gameInstance.scene.restart();

      setTimeout(() => {
        gameInstance.startGame();
      }, 100);

      return;
    }

    if (this.#currentGame) {
      this.scene.sleep(this.#currentGame);
    }

    let game = null;

    // find new game
    do {
      game = shuffle(this.#games)[0];

      if (!this.#currentGame || game !== this.#currentGame) {
        this.#currentGame = game;
        break;
      }
    } while (game === this.#currentGame);

    this.scene.get(BACKGROUND_KEY).resetBackgroundColor();
    this.#resetFullscreenButtonColor();
    this.input.setDefaultCursor('default');

    if (this.scene.isSleeping(this.#currentGame)) {
      this.scene.wake(this.#currentGame);
      this.scene.get(this.#currentGame).restartGame();
    } else {
      this.scene.launch(this.#currentGame, {
        preventStart: initialize,
      });
    }

    this.scene.get(DRAWING_TOOLS_KEY).reset();
    this.scene.bringToTop(this.key);
    this.scene.sendToBack(BACKGROUND_KEY);

    if (this.scene.isVisible(DRAWING_TOOLS_KEY)) {
      this.scene.bringToTop(DRAWING_TOOLS_KEY);
    }

    this.#updateSceneVolumes();

    const hintTimeout =
      (this.#started
        ? START_TIMER_SECONDS + HINT_TIMER_SECONDS
        : HINT_TIMER_SECONDS) * 1000;

    setTimeout(() => {
      if (this.scene.isActive(this.#currentGame)) {
        const currentGame = this.scene.get(this.#currentGame);

        if (!!currentGame.hintText && !currentGame.interacted) {
          const pos = currentGame.getHintPos();

          this.#hint
            .setText(currentGame.hintText)
            .setPosition(pos.x, pos.y)
            .show();
        }
      }
    }, hintTimeout);
  };

  hideHint = () => {
    this.#hint.hide();
  };

  setFullscreenButtonColor = (color) => {
    this.#fullscreenButton.setColor(color);
  };

  #resetFullscreenButtonColor = () => {
    this.#fullscreenButton.setColor('black');
  };

  #preventControls = () => !this.#started || this.#screenlock;

  #checkProgress = () => {
    if (this.#currentPoints >= POINTS_GOAL) {
      // TODO: win game!
      console.log('Won game!');
    }
  };

  #preloadGames = () => {
    // Note: turn games on/off by commenting out
    this.#games.push(SPRAY_BARS_KEY);
    preloadSprayBars(this);

    this.#games.push(SLICE_BARS_KEY);
    preloadSliceBars(this);

    this.#games.push(SLICE_CRACKERS_KEY);
    preloadSliceCrackers(this);

    this.#games.push(EAT_KEY);
    preloadEat(this);

    this.#games.push(ASTEROIDS_KEY);
    preloadAsteroids(this);
  };

  #createRadialProgressBar = () => {
    const radius = 60;
    const offset = 30;

    this.#radialProgressBar = new RadialProgressBar({
      scene: this,
      position: {
        x: this.scale.width - radius - offset,
        y: radius + offset,
      },
      radius,
      maxPoints: POINTS_GOAL,
      radialOffset: -90,
    });
  };

  #createFullscreenButton = (position) => {
    this.#fullscreenButton = new TextButton({
      scene: this,
      position,
      text: '\u{21F1}\n  \u{21F2}',
      style: {
        color: 'black',
        fontSize: isMobile(this) ? '55px' : '30px',
      },
      padding: {
        x: 20,
        y: 20,
      },
      onClick: () => {
        if (this.#preventControls()) {
          return;
        }

        if (this.scale.fullscreen.available) {
          if (this.scale.isFullscreen) {
            this.scale.stopFullscreen();
            store.dispatch(setFullscreen(false));
          } else {
            this.scale.startFullscreen();
            store.dispatch(setFullscreen(true));
          }
        }
      },
    });

    this.#controls.add(this.#fullscreenButton);
  };

  #createVolumeButton = (position) => {
    const volumeTexts = Object.freeze({
      0: '🔇',
      1: '🔈',
      2: '🔉',
      3: '🔊',
    });

    this.#volumeButton = new TextButton({
      scene: this,
      position,
      text: volumeTexts[this.#currentVolume],
      style: {
        fontSize: isMobile(this) ? '100px' : '60px',
      },
      padding: {
        x: 20,
        y: 20,
      },
      onClick: () => {
        if (this.#preventControls()) {
          return;
        }

        ++this.#currentVolume;

        if (this.#currentVolume > 3) {
          this.#currentVolume = 0;
        }

        this.#volumeButton.setText(volumeTexts[this.#currentVolume]);

        this.#updateSceneVolumes();
      },
    });

    this.#controls.add(this.#volumeButton);
  };

  #createCameraControlButton = (position) => {
    this.#cameraControlButton = new TextButton({
      scene: this,
      position,
      text: '🎥',
      style: {
        fontSize: isMobile(this) ? '100px' : '60px',
      },
      padding: {
        x: 20,
        y: 20,
      },
      border: {
        background: {
          radius: 10,
          alpha: 0.3,
        },
        visible: false,
      },
      onClick: () => {
        if (this.#preventControls()) {
          return;
        }

        const gameInstance = this.scene.get(this.#currentGame);
        gameInstance.pinchZoom = !gameInstance.pinchZoom;
        this.#cameraControlButton.setBorderVisible(gameInstance.pinchZoom);
      },
    });

    this.#cameraControlButton.setVisible(isMobile(this) ? true : false);

    this.#controls.add(this.#cameraControlButton.getButton());
  };

  #createGraphics = () => {
    const position = isMobile(this) ? { x: 125, y: 200 } : { x: 100, y: 100 };

    this.#createRadialProgressBar();
    this.#createFullscreenButton(position);
    this.#createVolumeButton(position);
    this.#createCameraControlButton(position);

    if (isMobile(this)) {
      this.#controls.incY(0, 200);
    } else {
      this.#controls.incX(0, 100);
    }
  };

  #updateSceneVolumes = () => {
    const scenes = this.scene.manager.getScenes();

    const volumeLevels = Object.freeze({
      0: 0,
      1: 1 / 3,
      2: 2 / 3,
      3: 1,
    });

    scenes.forEach((scene) => {
      const volume = volumeLevels[this.#currentVolume];

      if (volume === 0) {
        scene.sound.setMute(true);
      } else {
        scene.sound.setMute(false);
        scene.sound.setVolume(volume);
      }
    });
  };

  #createOrientationNotice = () => {
    if (isMobile(this)) {
      this.#lockOrientationScreen = this.add.container();

      const backgroundCover = this.make.graphics();

      backgroundCover
        .fillStyle(0xffffff, 0.85)
        .fillRect(0, 0, this.scale.width, this.scale.height);

      const lockText = this.add.text(
        this.scale.width - 490,
        250,
        'Rotate your device',
        {
          color: 'black',
          fontFamily: 'Courier',
          fontSize: '70px',
        }
      );

      lockText.setOrigin(0.5);

      const rotateImage = this.add.image(
        this.scale.width / 2 - 200,
        this.scale.height / 2 + 50,
        'rotatePhone'
      );

      this.#lockOrientationScreen
        .add([backgroundCover, rotateImage, lockText])
        .setDepth(1000)
        .setVisible(false);

      if (this.scale.isPortrait) {
        this.#lockOrientationScreen.setVisible(true);
      }

      this.scale.on('orientationchange', (orientation) => {
        if (orientation === Phaser.Scale.PORTRAIT) {
          this.#screenlock = true;

          if (this.#countdownTimer && !this.#countdownTimer.completed) {
            this.#countdownTimer.pause();
          }

          this.#lockOrientationScreen.setVisible(true);

          if (this.#currentGame) {
            this.scene.pause(this.#currentGame);
          }
        } else {
          this.#screenlock = false;

          if (this.#countdownTimer && !this.#countdownTimer.completed) {
            this.#countdownTimer.resume();
          }
          this.#lockOrientationScreen.setVisible(false);

          if (this.#currentGame) {
            this.scene.resume(this.#currentGame);
          }
        }
      });
    }
  };

  #startTimer = () => {
    const textPosition = {
      x: this.scale.width / 2,
      y: this.scale.height / 2 + TEXT_OFFSET_Y,
    };

    const textStyle = {
      fill: Theme.countdownTimer,
      fontSize: FONT_SIZE,
      fontFamily: FONT_FAMILY,
    };

    this.launchNextGame(true);

    this.#countdownTimer = new CountdownTimer({
      scene: this,
      position: textPosition,
      seconds: START_TIMER_SECONDS,
      style: textStyle,
      add: true,
      onComplete: () => {
        this.scene.get(this.#currentGame).startGame();
        this.#started = true;

        if (this.scene.isActive(DRAWING_TOOLS_KEY)) {
          this.scene.get(DRAWING_TOOLS_KEY).started = true;
        }

        this.#updateSceneVolumes();
      },
    });

    if (this.scale.isPortrait) {
      this.#countdownTimer.pause();
      this.#screenlock = true;
    }
  };
}

export default LearnFrax;
