/**
 * @fileoverview Slice Crackers Game
 */

import Phaser from 'phaser';

import SliceBase from 'src/Game/scenes/Base/SliceBase';
import MistakeHandling from 'src/Game/singletons/MistakeHandling';

export const KEY = 'SliceCrackers';
const LINE_COLOR = 0x4b3621; // cafe noir
const LINE_HINT_COLOR = 0x0000ff; // blue
const HINT_TEXT = 'Slice me!';

const sectionSrc = Object.freeze([
  {
    key: 'grahamCracker',
    src: 'assets/crackers/graham-cracker-small.png',
  },
  {
    key: 'wholeWheatCracker',
    src: 'assets/crackers/whole-wheat-cracker-big.png',
  },
]);

const soundsSrc = Object.freeze([
  {
    key: 'bread1',
    src: 'assets/sounds/bread/bread1.mp3',
  },
  {
    key: 'bread2',
    src: 'assets/sounds/bread/bread2.mp3',
  },
  {
    key: 'bread3',
    src: 'assets/sounds/bread/bread3.mp3',
  },
  {
    key: 'bread4',
    src: 'assets/sounds/bread/bread4.mp3',
  },
]);

export const preloadSliceCrackers = (scene) => {
  sectionSrc.forEach((section) => scene.load.image(section.key, section.src));
  soundsSrc.map((sound) => scene.load.audio(sound.key, sound.src));
};

class SliceCrackers extends SliceBase {
  constructor() {
    super(KEY, {
      sectionSrc,
      soundsSrc,
    });
  }

  init(data) {
    data.preloadAssets = preloadSliceCrackers;
    super.init({ ...data, resetGame: this.#resetGame });
  }

  preload() {
    super.preload();
    this.#traceHints = this.add.container();
  }

  create() {
    super.create();
    this.hintText = HINT_TEXT;
    this.#addMistakeHandling();
    this.#initControls();
  }

  update() {
    super.update();
    this.#draw();
  }

  #line = null;
  #lines = [];
  #traceHints = null;
  #traceHintsLength = null;

  generateInstructionText = () => {
    const textMap = {
      2: 'in half',
      3: 'into thirds',
      4: 'into fourths',
    };

    const text = `Cut ${textMap[this.currentDivider]}`;
    this.setInstructionText(text);
  };

  #addMistakeHandling = () => {
    const mistakes = [
      this.showTryAgainToast,
      () => {
        this.showKeepTryingToast();
        this.showHints(this.currentDivider);
      },
      () => {
        this.showToast('Trace the slices', 'blue');
        this.#showTraceHints();
      },
    ];
    MistakeHandling.add(KEY, mistakes);
  };

  #draw = () => {
    if (!this.#traceHints.length || !this.#traceHintsLength) {
      return;
    }

    this.#traceHints.getAll().forEach((hint) => {
      hint.clear();
      const length = this.#traceHintsLength.getValue();
      const lineShape = new Phaser.Geom.Line(0, 0, 0, length);
      hint.lineStyle(5, LINE_HINT_COLOR, 0.5).strokeLineShape(lineShape);
    });
  };

  #showTraceHints = () => {
    this.dotHints.getAll().forEach((hint) => {
      const lineHint = this.add.graphics();
      lineHint
        .lineStyle(5, LINE_HINT_COLOR, 0.5)
        .setPosition(hint.x, this.centerY - this.sliceObject.scaledHeight / 2);

      this.#traceHints.add(lineHint);
    });

    this.#traceHints.setDepth(10); // to show over the slice object

    this.#traceHintsLength = this.tweens.addCounter({
      from: 0,
      to: this.sliceObject.scaledHeight,
      ease: 'Linear',
      duration: 1000,
      onStart: () => {
        this.pickSound();
        this.currentSound.play();
        this.sliceObject.shake();
      },
      onComplete: () => {
        this.stopSounds();
        this.sliceObject.stopShake();
        this.#traceHintsLength.remove();
        this.#traceHintsLength = null;
      },
    });
  };

  #initControls = () => {
    this.input
      .on('pointerdown', this.#handlePointerDown)
      .on('pointerup', this.#handlePointerUp)
      .on('pointermove', this.#slice)
      .on('pointerout', this.#handlePointerUp);
  };

  #preventControls = () => {
    if (!this.started || this.pinchZoom) {
      return true;
    }

    return false;
  };

  #handlePointerDown = (pointer) => {
    if (this.#preventControls()) {
      return;
    }

    this.#line = this.add.graphics();
    this.#line.lineStyle(3, LINE_COLOR, 0.7).beginPath();

    if (this.sliceObject.isPointerOver()) {
      this.#line.moveTo(pointer.worldX, pointer.worldY);
    }
  };

  #handlePointerUp = () => {
    if (this.#preventControls()) {
      return;
    }

    if (this.#line) {
      this.#line.closePath();
      const cutIndex = this.verifyCut();

      if (cutIndex > -1) {
        this.#lines.push(this.#line);

        if (this.#lines.length === this.currentDivider - 1) {
          this.#traceHints.removeAll(true);
          this.falling = true;
          this.sliceObject.startFall();
          this.#lines.forEach((line) => line.destroy());
          this.#lines = [];
        }
      } else if (this.points.length) {
        this.#line.destroy();
        this.#line = null;
        MistakeHandling.handleMistake(KEY);
      }

      this.#line = null;
      this.resetPoints();
    }

    if (!this.#traceHintsLength) {
      this.stopSounds();
      this.sliceObject?.stopShake();
    }
  };

  #destroyLines = () => {
    this.#lines.forEach((line) => line.destroy());
    this.#lines = [];
  };

  #resetGame = () => {
    this.#destroyLines();
  };

  #slice = (pointer) => {
    if (this.#preventControls()) {
      return;
    }

    if (
      pointer.isDown &&
      this.sliceObject.isPointerOver() &&
      this.#line &&
      !this.falling
    ) {
      this.interacted = true;
      this.learnFraxScene.hideHint();

      if (this.#traceHints) {
        this.#traceHints.setDepth(this.#line.depth);
      }

      if (!this.currentSound?.isPlaying) {
        if (this.currentSound?.isPaused) {
          this.currentSound.resume();
        } else {
          this.pickSound();
          this.currentSound.play();
          this.sliceObject.shake();
        }
      }
      const pointerX = this.input.activePointer.worldX;
      const pointerY = this.input.activePointer.worldY;
      this.#line.lineTo(pointerX, pointerY);
      this.#line.strokePath();
      this.points.push({
        x: pointerX,
        y: pointerY,
      });
    } else if (!this.#traceHintsLength) {
      this.sliceObject?.stopShake();
    }
  };
}

export default SliceCrackers;
