/**
 * @fileoverview Eat Game
 */

import Phaser from 'phaser';
import shuffle from 'lodash/shuffle';

import Theme from 'src/Game/singletons/Theme';
import BaseGame from 'src/Game/scenes/Base/BaseGame';
import RoundFood from 'src/Game/components/RoundFood';
import TriangleAlien from 'src/Game/components/TriangleAlien';
import TextButton from 'src/Game/components/Buttons/TextButton';
import {
  FONT_SIZE,
  FONT_FAMILY,
  TEXT_OFFSET_Y,
} from 'src/Game/scenes/LearnFrax';
import {
  randomInt,
  randomEvenInt,
  offsetFromAnchor,
  getNewNumber,
  initPinchZoom,
  isMobile,
  verifyType,
} from 'src/lib/lib-game-utils';
import { Types } from 'src/Game/config/constants';
import MistakeHandling from 'src/Game/singletons/MistakeHandling';

export const KEY = 'Eat';
const PROGRESS_POINTS = 10;
const CURSOR = 'grab';
const BUTTON_COLOR = '#2bcda4';
const HINT_TEXT = 'Please feed me!';

const LEVELS = Object.freeze({
  1: Object.freeze({
    maxDenominator: 8,
    maxNumerator: 1,
  }),
  2: Object.freeze({
    maxDenominator: 8,
    maxNumerator: 1,
    pizzaCount: 4,
    minWidth: 200,
    maxWidth: 350,
    guideLineThickness: 6,
  }),
  3: Object.freeze({
    maxDenominator: 8,
    maxNumerator: 7,
    minDenominator: 3,
    minNumerator: 2,
  }),
  4: Object.freeze({
    maxDenominator: 8,
    maxNumerator: 7,
    pizzaCount: 4,
    minWidth: 200,
    maxWidth: 350,
    guideLineThickness: 6,
  }),
  5: Object.freeze({
    maxDenominator: 4,
    maxNumerator: 3,
    generateEquivalentFractions: true,
    minSlices: 8,
    maxSlices: 12,
    onlyEvenDivisions: false,
  }),
});

const foodSrc = Object.freeze([
  {
    key: 'pizza',
    src: 'assets/round/pizza.png',
  },
  {
    key: 'raspberry-pie',
    src: 'assets/round/raspberrypeach.png',
  },
  {
    key: 'cherry-pie',
    src: 'assets/round/cherrypie.png',
  },
]);

export const preloadEat = (scene) => {
  foodSrc.forEach((food) => scene.load.image(food.key, food.src));
  scene.load.audio('vacuum', 'assets/sounds/vacuum/vacuum.mp3');
};

class Eat extends BaseGame {
  constructor() {
    super(KEY);
  }

  init(data) {
    super.init(data, preloadEat);
    this.#level = data.startLevel ?? 1;
  }

  preload() {
    super.preload();
  }

  create() {
    this.resetCursor();
    this.hintText = HINT_TEXT;

    this.#createSounds();
    this.#createEatingPerson();
    this.#createDoneEatingButton();
    this.#addMistakeHandling();
    this.#initControls();

    this.#createLevel();

    super.create({
      instructionText: {
        offset: { x: 0, y: TEXT_OFFSET_Y },
        textStyle: {
          fill: Theme.countdownTimer,
          fontSize: FONT_SIZE,
          fontFamily: FONT_FAMILY,
        },
      },
    });
  }

  update() {
    this.#eatingPerson.animate();
  }

  get pinchZoom() {
    return this.#pinchZoom;
  }

  set pinchZoom(zoom) {
    if (!verifyType(Types.Boolean, zoom)) {
      return;
    }

    this.#pinchZoom = zoom;
  }

  #food = [];
  #eatingPerson = null;
  #mouth = null;
  #pupil = null;
  #eye = null;
  #vacuumSound = null;
  #numerator = null;
  #denominator = null;
  #sliceCount = null;
  #goal = null;
  #eatCount = 0;
  #button = null;
  #pinchZoom = false;
  #level = null;
  #verifyDenominators = [];
  #generateEquivalentFractions = null;
  #toast = null;
  #toastTimer = null;
  #stickyToast = null;

  resetCursor = () => {
    this.input.setDefaultCursor(CURSOR);
  };

  startGame = () => {
    this.#generateInstructionText();
    this.started = true;
  };

  restartGame = () => {
    this.resetCursor();
    this.#eatCount = 0;
    this.#food.forEach((item) => item.destroy());
    this.#food = [];
    this.#verifyDenominators = [];
    this.interacted = false;
    this.#clearToast();
    MistakeHandling.resetIndex(this.scene.key);
    this.#createLevel();
    this.#generateInstructionText();
  };

  getHintPos = () => {
    const x = this.#eatingPerson.x + this.#eatingPerson.length / 2 + 50;
    const y = this.#eatingPerson.y + this.#eatingPerson.length / 4;

    return { x, y };
  };

  #getLevelConfigs = () => LEVELS[this.#level];

  #preventControls = () => {
    if (!this.started || this.#pinchZoom) {
      return true;
    }

    return false;
  };

  #initControls = () => {
    this.input.on('drag', (pointer, gameObject, dragX, dragY) => {
      if (this.#preventControls()) {
        return;
      }

      if (gameObject.drag) {
        this.interacted = true;
        this.learnFraxScene.hideHint();
        gameObject.drag(dragX, dragY);
      }
    });

    this.input.on('dragenter', (pointer, gameObject, dropZone) => {
      if (this.#preventControls()) {
        return;
      }

      dropZone.dragEnter();
    });

    this.input.on('dragleave', (pointer, gameObject, dropZone) => {
      if (this.#preventControls()) {
        return;
      }

      dropZone.dragLeave();
    });

    this.input.on('drop', (pointer, gameObject, dropZone) => {
      if (this.#preventControls()) {
        return;
      }

      ++this.#eatCount;

      if (gameObject.shrinkFade && this.#checkEatAccuracy(gameObject)) {
        const duration = 400;

        this.#vacuumSound.play();
        dropZone.drop();

        gameObject.shrinkFade(
          duration,
          { x: dropZone.x, y: dropZone.y + 100 },
          () => {
            this.#verifyDenominators.push(gameObject.denominator);
            gameObject.destroy();
            dropZone.onDropComplete();

            if (!!this.#stickyToast) {
              const remainingFood = this.#remainingFood();

              if (remainingFood) {
                this.#stickyToast.setText(`Feed ${remainingFood} more slices`);
              } else {
                this.#stickyToast.setText(`Done eating!`);
              }
            }
          }
        );
      } else {
        --this.#eatCount;
        dropZone.onDropComplete();
      }
    });

    initPinchZoom(this);
  };

  #createSounds = () => {
    this.#vacuumSound = this.sound.add('vacuum', {
      seek: 15,
      rate: 3,
      volume: 1,
      loop: false,
    });
  };

  #createDoneEatingButton = () => {
    const buttonPosition = offsetFromAnchor(this.anchor, {
      x: 400,
      y: 300,
    });

    this.#button = new TextButton({
      scene: this,
      position: buttonPosition,
      style: {
        fontFamily: 'Courier',
        color: 'white',
        fontSize: isMobile(this) ? '50px' : '30px',
        backgroundColor: BUTTON_COLOR,
        align: 'center',
      },
      padding: {
        x: 20,
        y: 10,
      },
      border: {
        lineStyle: {
          lineWidth: 2,
          color: 0x2bcda4,
        },
        radius: 4,
      },
      text: 'Done Eating',
      onClick: this.#checkAccuracy,
      depth: 0,
    });
  };

  #addMistakeHandling = () => {
    const mistakes = [
      () => this.#showToast('Oops. Try again!'),
      () => this.#showToast('Keep Trying!'),
      () => {
        this.#showToast(
          `Feed ${this.#remainingFood()} more slices`,
          'blue',
          -1
        );
        const food = this.#food.find(
          (item) => item.denominator === this.#denominator
        );
        food.showHighlight();
      },
    ];
    MistakeHandling.add(KEY, mistakes);
  };

  #remainingFood = () => {
    const goalFood = this.#food.find(
      (item) => item.denominator === this.#denominator
    );

    if (goalFood) {
      return this.#goal - goalFood.eaten;
    }
  };

  #clearToast = () => {
    if (this.#toast) {
      this.#toast.destroy();
      this.#toast = null;

      if (this.#toastTimer) {
        clearTimeout(this.#toastTimer);
        this.#toastTimer = null;
      }
    }

    if (this.#stickyToast) {
      this.#stickyToast.destroy();
      this.#stickyToast = null;
    }
  };

  #showToast = (toastText, color = 'orange', duration = 2000) => {
    this.#clearToast();

    let pos;

    const { pizzaCount } = this.#getLevelConfigs();

    if (pizzaCount > 1) {
      pos = offsetFromAnchor(this.anchor, { x: 400, y: 400 });
    } else {
      pos = offsetFromAnchor(this.anchor, { x: -400, y: 325 });
    }

    if (duration > -1) {
      const [toast, timer] = MistakeHandling.showToast(
        this,
        pos,
        toastText,
        color,
        duration
      );

      this.#toast = toast;
      this.#toastTimer = timer;
    } else {
      this.#stickyToast = this.add.text(pos.x, pos.y, toastText, {
        color,
        fontSize: '50px',
        fontFamily: 'Courier',
      });

      this.#stickyToast.setOrigin(0.5);
    }
  };

  #checkEatAccuracy = (gameObject) => {
    if (!this.#eatCount) {
      return false;
    }

    const verified = gameObject.denominator === this.#denominator;

    if (verified) {
      const overAte = this.#eatCount > this.#goal;

      if (overAte) {
        this.#showToast('Done eating!');
        return false;
      }
      return true;
    } else {
      MistakeHandling.handleMistake(KEY);
    }

    return false;
  };

  #verifyDenoms = () => {
    if (!this.#generateEquivalentFractions) {
      for (const denom of this.#verifyDenominators) {
        if (denom !== this.#denominator) {
          return false;
        }
      }
    }

    return true;
  };

  #checkAccuracy = () => {
    if (!this.#eatCount) {
      return;
    }

    const verified = this.#verifyDenoms();
    const accurate = this.#goal === this.#eatCount && verified;

    if (accurate) {
      this.#level = Phaser.Math.Clamp(
        ++this.#level,
        1,
        Object.keys(LEVELS).length
      );

      this.learnFraxScene.addProgress(PROGRESS_POINTS);

      return true;
    } else {
      MistakeHandling.handleMistake(KEY);
      return false;
    }
  };

  #createFood = (config) => {
    const {
      introAnimation,
      minWidth = 400,
      maxWidth = 500,
      offsetPosition = { x: 0, y: 0 },
      slices = this.#sliceCount,
      guideLineThickness,
      generateEquivalentFractions = false,
    } = config;

    return new RoundFood({
      scene: this,
      texture: this.#getFood(),
      position: offsetFromAnchor(this.anchor, {
        x: -400 + offsetPosition.x,
        y: 0 + offsetPosition.y,
      }),
      maxWidth: randomInt(minWidth, maxWidth + 1),
      slices,
      depth: 1,
      guideLineThickness,
      introAnimation,
      denominator: generateEquivalentFractions ? this.#denominator : slices,
      goal: this.#goal,
      onGrab: () => {
        if (this.#preventControls()) {
          return;
        }

        this.#eatingPerson.setEyeOpen(true);
      },
      onDrop: () => {
        if (this.#preventControls()) {
          return;
        }

        this.#eatingPerson.setEyeOpen(false);
      },
    });
  };

  #createEatingPerson = () => {
    const length = 400;
    const { x, y } = offsetFromAnchor(this.anchor, { x: 400, y: -length / 2 });
    this.#eatingPerson = new TriangleAlien({ scene: this, x, y });
    this.#eatingPerson.setEyeMovement(true);
  };

  #generateInstructionText = () => {
    const text = `Feed ${this.#numerator}/${this.#denominator}`;
    this.setInstructionText(text);
  };

  #generateFraction = ({
    maxDenominator,
    maxNumerator,
    minDenominator = 2,
    minNumerator = 1,
    generateEquivalentFractions = false,
    minSlices = 2,
    maxSlices = 20,
    onlyEvenDivisions = false,
  }) => {
    this.#denominator = getNewNumber(this.#denominator, () =>
      onlyEvenDivisions
        ? randomEvenInt(minDenominator, maxDenominator + 1)
        : randomInt(minDenominator, maxDenominator + 1)
    );

    this.#numerator =
      maxNumerator > 1 ? randomInt(minNumerator, this.#denominator) : 1;

    if (this.#numerator > 1) {
      // reduce fraction
      while (true) {
        if (this.#numerator % 2 === 0 && this.#denominator % 2 === 0) {
          this.#numerator /= 2;
          this.#denominator /= 2;
        } else break;
      }
    }

    if (generateEquivalentFractions) {
      // generate slices and find target eat goal
      do {
        this.#sliceCount = onlyEvenDivisions
          ? randomEvenInt(minSlices, maxSlices + 1)
          : randomInt(minSlices, maxSlices + 1);

        this.#goal = (this.#sliceCount * this.#numerator) / this.#denominator;
      } while (this.#goal % 1 !== 0);
    } else {
      this.#sliceCount = this.#denominator;
      this.#goal = this.#numerator;
    }
  };

  #createLevel = () => {
    const {
      maxDenominator,
      maxNumerator,
      generateEquivalentFractions,
      minSlices,
      maxSlices,
      onlyEvenDivisions,
      pizzaCount = 1,
      minWidth = 400,
      maxWidth = 500,
      guideLineThickness,
      minDenominator,
      minNumerator,
    } = this.#getLevelConfigs();

    this.#generateEquivalentFractions = generateEquivalentFractions;

    this.#generateFraction({
      maxDenominator,
      maxNumerator,
      generateEquivalentFractions,
      minSlices,
      maxSlices,
      onlyEvenDivisions,
      minDenominator,
      minNumerator,
    });

    let denominators = [];
    if (pizzaCount > 1) {
      // generate random denominators - kind of a terrible way to do it
      denominators = Array.from({ length: maxDenominator }, (v, k) => k + 1);
      denominators.shift();
      const index = denominators.indexOf(this.#sliceCount);
      if (index > -1) {
        denominators.splice(index, 1);
      }
      denominators.push(this.#sliceCount);
      while (denominators.length > pizzaCount) {
        denominators.shift();
      }
      denominators = shuffle(denominators);
    } else {
      denominators.push(this.#sliceCount);
    }

    const alignByGrid = pizzaCount > 1;
    const gridSize = 400;

    let yShift = 0;
    denominators.forEach((slices, index) => {
      const isEven = index % 2 === 0;

      const food = this.#createFood({
        introAnimation: true,
        minWidth,
        maxWidth,
        slices,
        offsetPosition: alignByGrid
          ? {
              x: -120 + (isEven ? gridSize : 0),
              y: -100 + yShift,
            }
          : undefined,
        guideLineThickness,
        generateEquivalentFractions,
      });

      if (!isEven) {
        yShift += gridSize;
      }

      this.#food.push(food);
    });
  };

  #getFood = () => {
    const foodKeys = foodSrc.map((food) => food.key);
    return foodKeys[randomInt(0, foodKeys.length)];
  };
}

export default Eat;
