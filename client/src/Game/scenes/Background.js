import Phaser from 'phaser';

import Theme from 'src/Game/singletons/Theme';

export const KEY = 'Background';

const ALPHA = 0.15;

export const preloadBackground = (scene) => {
  scene.load.svg('graph', 'assets/graph/graph.svg');
};

class Background extends Phaser.Scene {
  constructor() {
    super(KEY);
  }

  init(data) {
    this.#preload = data.preload;
    this.#alpha = data.alpha || ALPHA;
  }

  preload() {
    if (this.#preload) {
      preloadBackground();
    }
  }

  create() {
    this.cameras.main.setBackgroundColor(Theme.background);

    this.#graph = this.add.tileSprite(
      20,
      20,
      this.scale.width * 2,
      this.scale.height * 2,
      'graph'
    );

    this.#graph.setAlpha(this.#alpha).setVisible(false);
  }

  #preload = null;
  #graph = null;
  #alpha = null;

  get graph() {
    return this.#graph;
  }

  setBackgroundColor = (color) => {
    this.cameras.main.setBackgroundColor(color);
  };

  resetBackgroundColor = () => {
    this.cameras.main.setBackgroundColor(Theme.background);
  };
}

export default Background;
