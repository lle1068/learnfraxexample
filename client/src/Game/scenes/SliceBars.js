/**
 * @fileoverview Slice Bars Game
 */

import Phaser from 'phaser';

import SliceBase from 'src/Game/scenes/Base/SliceBase';
import MistakeHandling from 'src/Game/singletons/MistakeHandling';

export const KEY = 'SliceBars';
const LINE_COLOR = 0xffffff;
const LINE_HINT_COLOR = 0xf0ffff; // azure
const HINT_TEXT = 'Cut me!';

const sectionSrc = Object.freeze([
  {
    key: 'blackBar',
    src: 'assets/bars/black-bar.png',
  },
  {
    key: 'bronzeBar',
    src: 'assets/bars/bronze-bar.png',
  },
  {
    key: 'greyBar',
    src: 'assets/bars/grey-bar.png',
  },
  {
    key: 'steelBar',
    src: 'assets/bars/steel-bar.png',
  },
]);

const soundsSrc = Object.freeze([
  {
    key: 'metalImpact',
    src: 'assets/sounds/metal/metal-impact.mp3',
  },
]);

export const preloadSliceBars = (scene) => {
  sectionSrc.forEach((section) => scene.load.image(section.key, section.src));
  soundsSrc.map((sound) => scene.load.audio(sound.key, sound.src));
};

class SliceBars extends SliceBase {
  constructor() {
    super(KEY, {
      sectionSrc,
      soundsSrc,
      horizontalTolerance: 20,
      maxDivider: 8,
      minWidth: 1000,
      maxWidth: 1500,
    });
  }

  init(data) {
    data.preloadAssets = preloadSliceBars;
    super.init({ ...data, resetGame: this.#resetGame });
  }

  preload() {
    super.preload();
  }

  create() {
    super.create();
    this.hintText = HINT_TEXT;
    this.#createLine();
    this.#addMistakeHandling();
    this.#initControls();
  }

  #line = null;
  #barFalling = false;
  #cutHint = null;

  #resetGame = () => {
    this.#barFalling = false;
    this.#createLine();
  };

  #destroyLine = () => {
    if (this.#line) {
      this.#line.destroy();
      this.#line = null;
    }
  };

  #createLine = () => {
    this.#destroyLine();

    const lineShape = new Phaser.Geom.Line(
      0,
      0,
      0,
      this.sliceObject.scaledHeight
    );

    this.#line = this.add.graphics();
    this.#line.lineStyle(3, LINE_COLOR, 0.6).strokeLineShape(lineShape);
    this.#line
      .setPosition(
        this.centerX,
        this.centerY - this.sliceObject.scaledHeight / 2
      )
      .setVisible(false);
  };

  #addMistakeHandling = () => {
    const mistakes = [
      this.showTryAgainToast,
      () => {
        this.showKeepTryingToast();
        this.showHints(this.currentDivider, 1);
      },
      () => {
        this.showToast('Cut the length', 'blue');
        this.#showCutHint();
      },
    ];
    MistakeHandling.add(KEY, mistakes);
  };

  #initControls = () => {
    this.input
      .on('pointerup', this.#handlePointerUp)
      .on('pointermove', this.#handleHover);
  };

  #preventControls = () => {
    if (!this.started || this.pinchZoom) {
      return true;
    }

    return false;
  };

  #showCutHint = () => {
    this.dotHints.getAll().forEach((hint) => {
      this.#cutHint = this.add.line(
        0,
        this.scale.height / 2,
        0,
        0,
        0,
        this.sliceObject.scaledHeight,
        LINE_HINT_COLOR,
        0.5
      );

      this.#cutHint.setLineWidth(2.5);

      this.tweens.add({
        targets: this.#cutHint,
        x: {
          from: this.centerX - this.sliceObject.scaledWidth / 2,
          to: hint.x,
        },
        ease: 'Linear',
        duration: 1000,
      });
    });
  };

  #handleHover = () => {
    if (!this.#line || this.#barFalling || this.#preventControls()) {
      return;
    }

    if (this.sliceObject.isPointerOver()) {
      this.#line.setVisible(true).setX(this.input.activePointer.worldX);
    } else {
      this.#line.setVisible(false);
    }
  };

  #handlePointerUp = (pointer) => {
    if (this.#preventControls()) {
      return;
    }

    if (this.#line.visible) {
      this.interacted = true;
      this.learnFraxScene.hideHint();
      this.resetPoints();

      this.points.push({
        x: this.input.activePointer.worldX,
        y: this.input.activePointer.worldY,
      });

      const cutIndex = this.verifyCut(false, true);

      if (cutIndex === 0 || cutIndex + 1 === this.currentDivider - 1) {
        if (this.#cutHint) {
          this.#cutHint.destroy();
          this.#cutHint = null;
        }
        this.#line.destroy();
        this.sound.play('metalImpact');
        this.#barFalling = true;

        if (cutIndex === 0) {
          this.sliceObject.startFall(cutIndex, 'left');
        } else {
          this.sliceObject.startFall(cutIndex + 1, 'right');
        }
      } else {
        MistakeHandling.handleMistake(KEY);
      }
    }
  };
}

export default SliceBars;
