import BaseGame from 'src/Game/scenes/Base/BaseGame';
import {
  FONT_SIZE,
  FONT_FAMILY,
  TEXT_OFFSET_Y,
} from 'src/Game/scenes/LearnFrax'; // example configs
import Theme from 'src/Game/singletons/Theme'; // global theme colors and configs
// import MistakeHandling from 'src/Game/singletons/MistakeHandling'; // include if handling mistakes

export const KEY = 'Gamekey';
const INSTRUCTION_TEXT = 'Instructions'; // instructions that display
const HINT_TEXT = 'Hint interaction text';

// TODO: rename this method
export const preloadAssets = (scene) => {
  // add preload methods here
  // scene.load...
};

class GameTemplate extends BaseGame {
  constructor() {
    super(KEY);
  }

  init(data) {
    super.init(data, preloadAssets);
  }

  preload() {
    super.preload();
  }

  create() {
    // all other necessary create methods

    // example instruction text creation
    // Note: call this after the above
    super.create({
      instructionText: {
        offset: { x: 0, y: TEXT_OFFSET_Y },
        textStyle: {
          fill: Theme.countdownTimer,
          fontSize: FONT_SIZE,
          fontFamily: FONT_FAMILY,
        },
      },
    });

    this.#addMistakeHandling();
    this.hintText = HINT_TEXT; // add hint text for interaction hinting (no interaction timeout)
  }

  update(time, delta) {
    // add update methods
  }

  // overload this method
  startGame = () => {
    this.setInstructionText(INSTRUCTION_TEXT);
    this.started = true;
  };

  // need this method for restarting a game that has been previously launched
  restartGame = () => {
    // TODO: add methods used to destroy/redraw
  };

  // method to reset cursor to previous settings after draw tools close
  resetCursor = () => {};

  // method to retrieve custom position of interation hints for displaying
  getHintPos = () => ({ x: this.centerX, y: this.centerY });

  // use to prevent user interaction before game has started
  #preventControls = () => {
    // add more conditions if needed
    if (!this.started) {
      return true;
    }

    return false;
  };

  // method to handle mistakes
  #addMistakeHandling = () => {
    // EXAMPLE
    // const mistakes = [
    //   this.showTryAgainToast,
    //   () => {
    //     this.showKeepTryingToast();
    //     this.showHints(this.currentDivider);
    //   },
    //   () => {
    //     this.showToast('Trace the slices', 'blue');
    //     this.#showTraceHints();
    //   },
    // ];
    // MistakeHandling.add(KEY, mistakes);
  };
}

export default GameTemplate;
