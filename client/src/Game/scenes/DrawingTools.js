import Phaser from 'phaser';

import { KEY as BACKGROUND_KEY } from 'src/Game/scenes/Background';
import { KEY as LEARN_FRAX_KEY } from 'src/Game/scenes/LearnFrax';

import TextButton from 'src/Game/components/Buttons/TextButton';
import { isMobile, isPositionWithinSelectionBox } from 'src/lib/lib-game-utils';
import isFinite from 'lodash/isFinite';

export const KEY = 'DrawingTools';
const SELECTION_BOX_COLOR = 0xcc0000;
const UNDO_BUTTON_COLOR = '#5e5e5e';
const ERASE_BUTTON_COLOR = '#5e5e5e';
const ACTIVE_ERASE_BUTTON_COLOR = 'red';
const BUTTON_COLORS = Object.freeze([
  0x0a0a0a, // off black - black doesn't work for some reason
  0x01a995, // green
  0xe84d39, // red
  0xca3a7c, // purple
  0x1865f2, // blue
]);

class DrawingTools extends Phaser.Scene {
  constructor() {
    super(KEY);
  }

  create() {
    this.#controls = this.add.group();
    this.#lines = this.add.container();
    this.#selectionBox = this.add.graphics();
    this.#controlPosition = {
      x: 100,
      y: this.scale.height - 515,
    };

    this.#createGraphics();
    this.#initControls();
  }

  update() {
    if (
      this.#eraseMode &&
      this.#drawMode &&
      this.scene.get(this.scene.get(LEARN_FRAX_KEY).currentGame).scene.isPaused
    ) {
      const pointer = this.input.activePointer;

      if (pointer.isDown) {
        const width = pointer.worldX - pointer.downX;
        const height = pointer.worldY - pointer.downY;

        this.#selectionBox
          .clear()
          .fillStyle(SELECTION_BOX_COLOR, 0.3)
          .fillRect(0, 0, width, height)
          .lineStyle(2, SELECTION_BOX_COLOR, 0.4)
          .strokeRect(0, 0, width, height);

        this.#selectionBox.setPosition(pointer.downX, pointer.downY);
        this.#selectionBox.width = width; // allow negative
        this.#selectionBox.height = height; // allow negative
      } else {
        if (this.#lines.length && isFinite(this.#selectionBox.width)) {
          const collisions = [];
          const lines = this.#lines.getAll();

          for (const line of lines) {
            if (!line.visible) {
              continue; // skip hidden lines
            }

            let collided = false;

            for (const point of line.polygon.points) {
              if (isPositionWithinSelectionBox(point, this.#selectionBox)) {
                collided = true;
                break;
              }
            }

            if (collided) {
              line.setVisible(false);
              collisions.push(line);
            }
          }

          if (collisions.length) {
            this.#actionStack.push(collisions);
          }
        }

        this.#selectionBox.clear();
        this.#selectionBox.width = null;
        this.#selectionBox.height = null;
      }
    }
  }

  #started = false;
  #viewButton = null;
  #view = true;
  #drawButton = null;
  #undoButton = null;
  #eraseButton = null;
  #eraseMode = false;
  #drawMode = false;
  #controls = null;
  #currentColor = null;
  #lines = null;
  #line = null;
  #actionStack = [];
  #selectionBox = null;
  #points = null;
  #controlPosition = null;

  set started(started) {
    if (typeof started !== 'boolean') {
      console.log('Error: started must be of type boolean');
      return;
    }

    this.#started = started;
  }

  reset = () => {
    if (this.#started) {
      this.#selectionBox.clear();
      this.#actionStack = [];
      this.#lines.setVisible(true).removeAll(true);
      this.#currentColor = null;
      this.#clearCheckmarks();
      this.#eraseMode = false;
      this.#eraseButton.setColor(ERASE_BUTTON_COLOR);
    }
  };

  #isControlHover = () => {
    const controls = this.#controls.getChildren();

    if (this.#viewButton.hover || this.#drawButton.hover) {
      return true;
    }

    for (const container of controls) {
      if (container.getFirst('type', 'Text').hover) {
        return true;
      }
    }

    return false;
  };

  #preventControls = () => {
    if (!this.#started || !this.#drawMode) {
      return true;
    }

    return false;
  };

  #initControls = () => {
    this.input
      .on('pointerdown', (pointer) => {
        if (this.#preventControls() || this.#eraseMode) {
          return;
        }

        if (
          this.#currentColor &&
          !this.#isControlHover() &&
          !pointer.rightButtonDown()
        ) {
          this.#actionStack.push('draw');

          this.#line = this.add.graphics();
          this.#line
            .lineStyle(3, this.#currentColor)
            .beginPath()
            .moveTo(pointer.worldX, pointer.worldY);

          this.#points = [];
          this.#points.push({ x: pointer.worldX, y: pointer.worldY });
        }
      })
      .on('pointermove', (pointer) => {
        if (this.#preventControls()) {
          return;
        }

        if (pointer.isDown && this.#line) {
          this.#line.lineTo(pointer.worldX, pointer.worldY).strokePath();
          this.#points.push({ x: pointer.worldX, y: pointer.worldY });
        }
      })
      .on('pointerup', () => {
        if (this.#preventControls()) {
          return;
        }

        if (this.#line && this.#drawMode) {
          this.#line.closePath();

          const polygon = new Phaser.Geom.Polygon(this.#points);
          this.#points = null;
          this.#line.polygon = polygon;

          this.#lines.add(this.#line);
          this.#line = null;
        }
      });
  };

  #createDrawButton = () => {
    this.#drawButton = new TextButton({
      scene: this,
      position: {
        x: 100,
        y: this.scale.height,
      },
      text: '✍️',
      style: {
        fontSize: isMobile(this) ? '100px' : '60px',
      },
      padding: {
        x: 20,
        y: 20,
      },
      onClick: () => {
        if (!this.#started) {
          return;
        }

        this.#drawMode = !this.#drawMode;
        this.#controls.setVisible(this.#drawMode);

        const graph = this.scene.get(BACKGROUND_KEY).graph;
        graph.setVisible(!graph.visible);

        const currentGameKey = this.scene.get(LEARN_FRAX_KEY).currentGame;

        if (this.#drawMode) {
          // default select black color
          this.#clearCheckmarks();

          this.#eraseMode = false;
          this.#eraseButton.setColor(ERASE_BUTTON_COLOR);

          const black = BUTTON_COLORS[0];
          this.#currentColor = black;

          const blackButton = this.#controls
            .getChildren()[2]
            .getFirst('type', 'Text');
          blackButton.setText(this.#currentColor ? '\u{2713}' : ''); // white checkmark

          this.scene.pause(currentGameKey);
          this.input.setDefaultCursor('default');

          if (this.#lines.length) {
            this.#lines.setVisible(true);
            this.#viewButton.setAlpha(0.9);
          }
        } else {
          this.scene.run(currentGameKey);
          const currentGame = this.scene.get(currentGameKey);
          if (currentGame.resetCursor) {
            currentGame.resetCursor();
          }

          if (this.#lines.length) {
            this.#lines.setVisible(this.#view);
            this.#viewButton.setAlpha(this.#view ? 0.9 : 0.3);
          }
        }
      },
    });
  };

  #createViewButton = () => {
    this.#viewButton = new TextButton({
      scene: this,
      position: {
        x: 200,
        y: this.scale.height,
      },
      text: '\u{1f441}', // eye
      style: {
        fontSize: isMobile(this) ? '100px' : '60px',
      },
      padding: {
        x: 20,
        y: 20,
      },
      alpha: 0.9,
      onClick: () => {
        if (!this.#started) {
          return;
        }

        if (this.#lines.length && !this.#drawMode) {
          this.#view = !this.#view;
          this.#lines.setVisible(this.#view);
          this.#viewButton.setAlpha(this.#view ? 0.9 : 0.3);
        }
      },
    });
  };

  #createUndoButton = () => {
    this.#undoButton = new TextButton({
      scene: this,
      name: 'undoButton',
      position: this.#controlPosition,
      text: '\u{27F2}',
      style: {
        color: UNDO_BUTTON_COLOR,
        fontSize: '50px',
      },
      padding: {
        x: 20,
        y: 10,
      },
      onClick: () => {
        if (this.#preventControls()) {
          return;
        }

        const action = this.#actionStack.pop();

        if (action === 'draw' && this.#lines.length) {
          this.#lines.removeAt(this.#lines.length - 1, true);
        } else if (Array.isArray(action)) {
          const lines = action;
          lines.forEach((line) => line.setVisible(true));
        }
      },
    });

    this.#undoButton.getButton().setX(-2.5);
    this.#controls.add(this.#undoButton.getButton());
  };

  #createEraseButton = () => {
    this.#eraseButton = new TextButton({
      scene: this,
      name: 'eraseButton',
      position: this.#controlPosition,
      text: '\u{2B1A}',
      style: {
        color: ERASE_BUTTON_COLOR,
        fontSize: '60px',
      },
      background: {
        alpha: 0.3,
        radius: 5,
        visible: this.#eraseMode,
      },
      padding: {
        x: 13,
        y: 2,
      },
      onClick: () => {
        if (!this.#started || !this.#drawMode) {
          return;
        }

        this.#currentColor = null;
        this.#clearCheckmarks();

        this.#eraseMode = !this.#eraseMode;
        this.#eraseButton.setColor(
          this.#eraseMode ? ACTIVE_ERASE_BUTTON_COLOR : ERASE_BUTTON_COLOR
        );
      },
    });

    this.#eraseButton.getButton().setX(-2.5);
    this.#controls.add(this.#eraseButton.getButton());
  };

  #createColorButtons = () => {
    BUTTON_COLORS.forEach((color) => {
      const button = new TextButton({
        scene: this,
        position: this.#controlPosition,
        text: '',
        style: {
          fontSize: '35px',
        },
        circle: {
          color,
          radius: 20,
          offsetX: 18,
          offsetY: 27,
        },
        padding: {
          x: 20,
          y: 10,
        },
        onClick: () => {
          if (this.#preventControls()) {
            return;
          }

          this.#currentColor = this.#currentColor === color ? null : color;

          this.#clearCheckmarks();

          this.#eraseMode = false;
          this.#eraseButton.setColor(ERASE_BUTTON_COLOR);

          button.setText(this.#currentColor ? '\u{2713}' : ''); // white checkmark
        },
      });

      this.#controls.add(button.getButton());
    });
  };

  #createGraphics = () => {
    this.#createDrawButton();
    this.#createViewButton();
    this.#createUndoButton();
    this.#createEraseButton();
    this.#createColorButtons();

    this.#controls.setVisible(false).incY(0, 62);
  };

  #clearCheckmarks = () => {
    if (this.#controls.getLength()) {
      this.#controls.getChildren().forEach((child) => {
        const button = child.getFirst('type', 'Text');

        if (button.name !== 'undoButton' && button.name !== 'eraseButton') {
          button.setText('');
        }
      });
    }
  };
}

export default DrawingTools;
