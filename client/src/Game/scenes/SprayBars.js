/**
 * @fileoverview Color Bars Game
 */

import isEmpty from 'lodash/isEmpty';

import SliceBase from 'src/Game/scenes/Base/SliceBase';
import MistakeHandling from 'src/Game/singletons/MistakeHandling';

export const KEY = 'SprayBars';
const SPRAY_COLOR = 0x3a5aff;
const LINE_HINT_COLOR = 0xf0ffff; // azure
const HINT_TEXT = 'Color me!';
const HORIZONTAL_TOLERANCE = 30;
const VERTICAL_TOLERANCE = 15;

const sectionSrc = Object.freeze([
  {
    key: 'steelPlate',
    src: 'assets/bars/steel-plate.png',
  },
]);

const soundsSrc = Object.freeze([
  {
    key: 'spray',
    src: 'assets/sounds/spray/spray.wav',
  },
]);

export const preloadSprayBars = (scene) => {
  sectionSrc.forEach((section) => scene.load.image(section.key, section.src));
  soundsSrc.map((sound) => scene.load.audio(sound.key, sound.src));

  scene.load.image('brush', 'assets/brush/brush1.png');
};

class SprayBars extends SliceBase {
  constructor() {
    super(KEY, {
      sectionSrc,
      soundsSrc,
      horizontalTolerance: HORIZONTAL_TOLERANCE,
      maxDivider: 8,
      minWidth: 1000,
      maxWidth: 1500,
      cursor: 'url(assets/cursor/spray-can.cur) 0 32, pointer',
      instructText: 'Color',
    });
  }

  init(data) {
    data.preloadAssets = preloadSprayBars;
    super.init({ ...data, resetGame: this.#resetGame });
  }

  preload() {
    super.preload();
  }

  create() {
    super.create();
    this.#sprayObject = this.sliceObject;
    this.hintText = HINT_TEXT;
    this.#spraySound = this.sound.add('spray', { loop: true });
    this.#createColorSpray();
    this.#addMistakeHandling();
    this.#initControls();
  }

  #rt = null;
  #sprayObject = null;
  #sprayHintLine = null;
  #spraySound = null;
  #sprayZone = {};

  #resetGame = () => {
    this.#sprayObject = this.sliceObject;
    this.#createColorSpray();
  };

  #createColorSpray = () => {
    if (this.#rt) {
      this.#rt.destroy();
      this.#rt = null;
    }

    this.#rt = this.add.renderTexture(
      this.centerX - this.#sprayObject.scaledWidth / 2,
      this.centerY - this.#sprayObject.scaledHeight / 2,
      this.#sprayObject.scaledWidth,
      this.#sprayObject.scaledHeight
    );

    this.#rt.setTint(SPRAY_COLOR);
  };

  #addMistakeHandling = () => {
    const mistakes = [
      this.showTryAgainToast,
      () => {
        this.showKeepTryingToast();
        this.showHints(this.currentDivider, 1);
      },
      () => {
        this.showToast('Color the length', 'blue');
        this.#showsprayHintLine();
      },
    ];
    MistakeHandling.add(KEY, mistakes);
  };

  #initControls = () => {
    this.input
      .on('pointerdown', this.#handlePointerDown)
      .on('pointerup', this.#handlePointerUp)
      .on('pointermove', this.#handleHover);
  };

  #preventControls = () => {
    if (!this.started || this.pinchZoom) {
      return true;
    }

    return false;
  };

  #showsprayHintLine = () => {
    this.dotHints.getAll().forEach((hint) => {
      this.#sprayHintLine = this.add.line(
        0,
        this.centerY,
        0,
        0,
        0,
        this.sliceObject.scaledHeight,
        LINE_HINT_COLOR,
        0.5
      );

      this.#sprayHintLine.setLineWidth(2.5);

      this.tweens.add({
        targets: this.#sprayHintLine,
        x: {
          from: this.centerX - this.sliceObject.scaledWidth / 2,
          to: hint.x,
        },
        ease: 'Linear',
        duration: 1000,
      });
    });
  };

  #handlePointerDown = () => {
    if (this.#preventControls()) {
      return;
    }

    if (this.#sprayObject.isPointerOver()) {
      this.interacted = true;
      this.learnFraxScene.hideHint();
      this.#spraySound.play();
      this.#spray();
    }
  };

  #handleHover = () => {
    if (this.#preventControls()) {
      return;
    }

    const activePointer = this.input.activePointer;

    if (activePointer.isDown) {
      if (this.#sprayObject.isPointerOver()) {
        if (this.#spraySound.isPaused) {
          this.#spraySound.resume();
        } else if (!this.#spraySound.isPlaying) {
          this.#spraySound.play();
        }

        this.interacted = true;
        this.#spray();
      } else if (this.#spraySound.isPlaying) {
        this.#spraySound.pause();
      }
    }
  };

  #spray = () => {
    const activePointer = this.input.activePointer;

    this.#rt.draw(
      'brush',
      activePointer.worldX -
        this.centerX +
        this.#sprayObject.scaledWidth / 2 -
        32,
      activePointer.worldY - this.scale.height / 2 + 32
    );

    const top = activePointer.worldY - 16;
    const bottom = activePointer.worldY + 16;
    const left = activePointer.worldX - 16;
    const right = activePointer.worldX + 16;

    this.#sprayZone.top =
      top < this.#sprayZone.top || !this.#sprayZone.top
        ? top
        : this.#sprayZone.top;
    this.#sprayZone.bottom =
      bottom > this.#sprayZone.bottom || !this.#sprayZone.bottom
        ? bottom
        : this.#sprayZone.bottom;
    this.#sprayZone.left =
      left < this.#sprayZone.left || !this.#sprayZone.left
        ? left
        : this.#sprayZone.left;
    this.#sprayZone.right =
      right > this.#sprayZone.right || !this.#sprayZone.right
        ? right
        : this.#sprayZone.right;
  };

  #handlePointerUp = (pointer) => {
    this.#spraySound.stop();

    if (this.#preventControls()) {
      return;
    }

    if (!isEmpty(this.#sprayZone)) {
      const verified = this.#verifySpray();

      if (verified) {
        this.#spraySound.stop();

        const points = {
          0: this.progressPoints,
          1: this.progressPoints,
          2: this.progressPoints / 2,
        };

        const mistakeIndex = MistakeHandling.getIndex(this.scene.key);
        this.learnFraxScene.addProgress(points[mistakeIndex]);
      } else {
        this.#sprayZone = {};
        this.#rt.clear();
        MistakeHandling.handleMistake(KEY);
      }
    }
  };

  #verifySpray = () => {
    const leftBound = this.centerX - this.#sprayObject.scaledWidth / 2;
    const rightBound =
      leftBound + this.#sprayObject.scaledWidth / this.currentDivider;
    const topBound = this.centerY - this.#sprayObject.scaledHeight / 2;
    const bottomBound = this.centerY + this.#sprayObject.scaledHeight / 2;

    return (
      this.#sprayZone.left <= leftBound + HORIZONTAL_TOLERANCE &&
      this.#sprayZone.right <= rightBound - 16 + HORIZONTAL_TOLERANCE &&
      this.#sprayZone.right >= rightBound - 16 - HORIZONTAL_TOLERANCE &&
      this.#sprayZone.top <= topBound + VERTICAL_TOLERANCE &&
      this.#sprayZone.bottom >= bottomBound - VERTICAL_TOLERANCE
    );
  };
}

export default SprayBars;
