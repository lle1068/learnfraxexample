/**
 * @fileoverview Main game setup
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Phaser from 'phaser';
import { useSelector } from 'react-redux';

import config from 'src/Game/config';

let game = null;
export const initGame = () => {
  if (game === null) {
    game = new Phaser.Game(config);
  }
};

export const getGameInstance = () => game;

export const endGame = () => {
  if (game) {
    // Strange destroy Phaser bug that requires these manual steps before destroy
    // Bug is possibly related to multiple scenes running at one time
    game.renderer.destroy();
    game.loop.stop();
    game.canvas.remove();
    // -------------------------------------------------------------------

    game.destroy(true, true);
    game = null;
  }
};

const StyledGameContainer = styled.div`
  background-color: ${(props) =>
    props.isPlaying && props.isLoaded
      ? props.theme.colors.black
      : props.theme.colors.black};
  height: ${(props) => (props.startTransition ? '100%' : 0)};
  width: ${(props) => (props.startTransition ? '100%' : 0)};
  opacity: ${(props) => (props.startTransition ? 1 : 0)};
  border-radius: 0.5rem;

  transition: height 1.5s ease-in-out, width 1.5s ease-in-out,
    opacity 2s ease-in-out;
`;

const Game = (props) => {
  Game.propTypes = {
    startTransition: PropTypes.bool,
    startGame: PropTypes.func,
  };

  const isPlaying = useSelector((state) => state.game.isPlaying);
  const isLoaded = useSelector((state) => state.game.isLoaded);

  return (
    <StyledGameContainer
      id='game-root'
      startTransition={props.startTransition}
      onTransitionEnd={props.startGame}
      isPlaying={isPlaying}
      isLoaded={isLoaded}
    />
  ); // game canvas container
};

export default Game;
