/**
 * @fileoverview Game configurations
 */

import Phaser from 'phaser';

import LearnFrax from 'src/Game/scenes/LearnFrax';
import DrawingTools from 'src/Game/scenes/DrawingTools';
import Background from 'src/Game/scenes/Background';
import SliceCrackers from 'src/Game/scenes/SliceCrackers';
import SliceBars from 'src/Game/scenes/SliceBars';
import SprayBars from 'src/Game/scenes/SprayBars';
import Eat from 'src/Game/scenes/Eat';
import Asteroids from 'src/Game/scenes/Asteroids';

export const resolution = {
  width: 1920,
  height: 1080,
};

const config = {
  type: Phaser.AUTO,
  parent: 'game-root',
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: resolution.width,
    height: resolution.height,
  },
  physics: {
    default: 'arcade',
  },
  version: '0.10.0',
  title: 'Learn Fractions!',
  scene: [
    LearnFrax,
    DrawingTools,
    Background,
    SliceCrackers,
    SliceBars,
    SprayBars,
    Eat,
    Asteroids,
  ],
};

export default config;
