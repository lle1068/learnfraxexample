import { Toast } from 'phaser3-rex-plugins/templates/ui/ui-components.js';

class MistakeHandling {
  constructor() {
    if (!MistakeHandling.instance) {
      MistakeHandling.instance = this;
    }

    return MistakeHandling.instance;
  }

  #mistakesMap = {};

  add = (key, mistakes = []) => {
    this.#mistakesMap[key] = {
      index: 0,
      mistakes,
    };
  };

  resetIndex = (key) => {
    if (this.#mistakesMap[key]?.index) {
      this.#mistakesMap[key].index = 0;
    }
  };

  getIndex = (key) => {
    const mistake = this.#mistakesMap[key] || {};
    return mistake.hasOwnProperty('index') ? mistake.index : -1;
  };

  handleMistake = (key) => {
    const mistake = this.#mistakesMap[key];

    if (mistake.index < mistake.mistakes.length) {
      mistake.mistakes[mistake.index]();
    }

    ++mistake.index;
  };

  showToast = (
    scene,
    position,
    toastText,
    color = 'orange',
    duration = 2000
  ) => {
    const text = scene.add.text(0, 0, '', {
      fontSize: '50px',
      fontFamily: 'Courier',
      color,
    });

    const toast = new Toast(scene, {
      x: position.x,
      y: position.y,
      text,
      duration: {
        in: 300,
        hold: duration,
        out: 300,
      },
      transitIn: 'fadeIn',
      transitOut: 'fadeOut',
    });

    toast.show(toastText);

    const timer = setTimeout(() => {
      if (toast) {
        toast.destroy();
      }
      text.destroy();
    }, duration + 600);

    return [toast, timer];
  };
}

const instance = new MistakeHandling();
Object.freeze(instance);

export default instance;
