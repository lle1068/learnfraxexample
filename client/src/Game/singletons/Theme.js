const Theme = Object.freeze({
  countdownTimer: '#4146C5',
  background: '#BFEFFF',
});

export default Theme;
