import Phaser from 'phaser';
import ShakePosition from 'phaser3-rex-plugins/plugins/shakeposition.js';

import { drawBoundCircularMovement } from 'src/lib/lib-game-utils';

// CONFIG
const MOUTH_OPEN_SIZE = 65;
const MOUTH_WIDTH = 200;
const MOUTH_TWEEN_DURATION = 200;
const EYE_SIZE = 60;
const DEBUG = false;

class TriangleAlien {
  constructor(config) {
    const {
      scene,
      x,
      y,
      backgroundColor = 0x000000,
      foregroundColor = 0xffffff,
      introAnimation = true,
    } = config;

    const offset = 150;

    this.#debug = DEBUG;
    this.#scene = scene;
    this.#pupilPosition = { x, y: y + offset };
    this.#backgroundColor = backgroundColor;
    this.#foregroundColor = foregroundColor;
    this.#introAnimation = introAnimation;
    this.#position = { x, y };
    this.#length = 400;

    this.#createAlien(x, y, this.#length);
    this.#createDropZone(x, y + offset + 25, this.#length, this.#length - 50);
  }

  get x() {
    return this.#position.x;
  }

  get y() {
    return this.#position.y;
  }

  get length() {
    return this.#length;
  }

  #scene = null;
  #dropZone = null;
  #pupilPosition = null;
  #container = null;
  #backgroundColor = null;
  #foregroundColor = null;
  #eye = null;
  #pupil = null;
  #mouth = null;
  #eyeSize = EYE_SIZE;
  #eyeOpen = false;
  #mouthWidth = MOUTH_WIDTH;
  #mouthHeight = null;
  #allowEyeMovement = true;
  #mouthAlpha = null;
  #pupilShake = null;
  #keepEyeOpen = false;
  #introAnimation = null;
  #triangleShape = null;
  #triangleBody = null;
  #debug = false;
  #position = null;
  #length = null;

  animate = () => {
    this.#drawEye();
    this.#drawMouth();
    if (this.#introAnimation) this.#drawRotation();
  };

  setEyeMovement = (movement) => {
    if (typeof movement === 'boolean') {
      this.#allowEyeMovement = movement;
    } else {
      console.log('Error: setEyeMovement parameter must be of type be boolean');
    }
  };

  setEyeOpen = (open) => {
    if (typeof open === 'boolean') {
      this.#eyeOpen = open;
    } else {
      console.log('Error: setEyeOpen parameter must be of type be boolean');
    }
  };

  destroy = () => {
    this.#container.destroy();
    this.#container = null;
    this.#eye.destroy();
    this.#eye = null;
    this.#pupil.destroy();
    this.#pupil = null;
    this.#mouth.destroy();
    this.#mouth = null;
    this.#dropZone.destroy();
    this.#dropZone = null;
  };

  #drawEye = () => {
    if (this.#eyeOpen || this.#keepEyeOpen) {
      this.#openEye();
    } else {
      this.#closeEye();
    }
  };

  #drawMouth = () => {
    const rectangleShape = new Phaser.Geom.Rectangle(
      0,
      0,
      this.#mouthWidth,
      this.#mouthHeight?.getValue() || 0
    );
    this.#mouth
      .clear()
      .lineStyle(2, this.#foregroundColor, this.#mouthAlpha?.getValue() || 0)
      .strokeRectShape(rectangleShape);
  };

  #openMouth = () => {
    this.#mouthAlpha = this.#scene.tweens.addCounter({
      from: 0,
      to: 1,
      ease: 'Power2',
      duration: MOUTH_TWEEN_DURATION,
    });

    this.#mouthHeight = this.#scene.tweens.addCounter({
      from: 0,
      to: MOUTH_OPEN_SIZE,
      ease: 'Power2',
      duration: MOUTH_TWEEN_DURATION,
    });
  };

  #closeMouth = () => {
    this.#mouthAlpha = this.#scene.tweens.addCounter({
      from: 1,
      to: 0,
      ease: 'Power2',
      duration: MOUTH_TWEEN_DURATION,
    });

    this.#mouthHeight = this.#scene.tweens.addCounter({
      from: MOUTH_OPEN_SIZE,
      to: 0,
      ease: 'Power2',
      duration: MOUTH_TWEEN_DURATION,
    });
  };

  #dragEnter = () => {
    this.#openMouth();
  };

  #dragLeave = () => {
    this.#closeMouth();
  };

  #openEye = () => {
    const eyeShape = new Phaser.Geom.Ellipse(
      0,
      0,
      this.#eyeSize,
      this.#eyeSize
    );
    this.#eye
      .clear()
      .fillStyle(this.#foregroundColor)
      .fillEllipseShape(eyeShape);

    this.#pupil.setVisible(true);

    if (this.#allowEyeMovement) {
      drawBoundCircularMovement(this.#scene.input, this.#eye, this.#pupil, 15);
    }
  };

  #closeEye = () => {
    this.#pupil
      .setPosition(this.#pupilPosition.x, this.#pupilPosition.y)
      .setVisible(false);

    const eyeShape = new Phaser.Geom.Ellipse(0, 0, this.#eyeSize, 2);
    this.#eye
      .clear()
      .fillStyle(this.#foregroundColor)
      .fillEllipseShape(eyeShape);
  };

  #createAlien = (x, y, length) => {
    this.#container = this.#scene.add.container();

    this.#triangleShape = Phaser.Geom.Triangle.BuildEquilateral(0, 0, length);

    this.#triangleBody = this.#scene.add.graphics();
    this.#triangleBody
      .lineStyle(2, this.#foregroundColor)
      .strokeTriangleShape(this.#triangleShape)
      .fillStyle(this.#backgroundColor)
      .fillTriangleShape(this.#triangleShape)
      .setAlpha(0.9);

    const eyeShape = new Phaser.Geom.Ellipse(
      0,
      0,
      this.#eyeSize,
      this.#eyeSize
    );
    this.#eye = this.#scene.add.graphics();
    this.#eye
      .lineStyle(2, this.#foregroundColor)
      .strokeEllipseShape(eyeShape)
      .fillStyle(this.#foregroundColor)
      .fillEllipseShape(eyeShape)
      .setPosition(x, y + 150);

    const pupilShape = new Phaser.Geom.Ellipse(
      0,
      0,
      this.#eyeSize / 3,
      this.#eyeSize / 3
    );
    this.#pupil = this.#scene.add.graphics();
    this.#pupil
      .lineStyle(2, this.#backgroundColor)
      .strokeEllipseShape(pupilShape)
      .fillStyle(this.#backgroundColor)
      .fillEllipseShape(pupilShape)
      .setPosition(x, y + 150);

    this.#pupilShake = new ShakePosition(this.#pupil);

    this.#mouth = this.#scene.add.graphics();
    const rectangleShape = new Phaser.Geom.Rectangle(0, 0, this.#mouthWidth, 0);
    this.#mouth
      .lineStyle(2, this.#foregroundColor)
      .strokeRectShape(rectangleShape)
      .fillStyle(this.#foregroundColor)
      .fillRectShape(rectangleShape)
      .setY(length / 2 + 40)
      .setX(-this.#mouthWidth / 2);

    this.#container.add([this.#triangleBody, this.#mouth]).setPosition(x, y);

    if (this.#introAnimation) {
      this.#eye.setAlpha(0);

      const tweenIntro = this.#scene.tweens.add({
        targets: this.#triangleBody,
        alpha: { from: 0, to: 1 },
        duration: 1000,
        x: { from: '+=600', to: this.#triangleBody.x },
        y: { from: '-=400', to: this.#triangleBody.y },
        ease: 'Linear',
        onComplete: () => {
          tweenIntro.remove();

          // reset triangle
          this.#triangleShape = Phaser.Geom.Triangle.BuildEquilateral(
            0,
            0,
            length
          );

          this.#triangleBody
            .clear()
            .lineStyle(2, this.#foregroundColor)
            .strokeTriangleShape(this.#triangleShape)
            .fillStyle(this.#backgroundColor)
            .fillTriangleShape(this.#triangleShape)
            .setAlpha(0.9);

          this.#introAnimation = false;
          this.#eye.setAlpha(1);
        },
      });
    }
  };

  #drawRotation = () => {
    Phaser.Geom.Triangle.Rotate(this.#triangleShape, -0.2);

    this.#triangleBody
      .clear()
      .lineStyle(2, this.#foregroundColor)
      .strokeTriangleShape(this.#triangleShape)
      .fillStyle(this.#backgroundColor)
      .fillTriangleShape(this.#triangleShape);
  };

  #drop = () => {
    this.#keepEyeOpen = true;
    this.#allowEyeMovement = false;
    this.#pupil.setPosition(this.#pupilPosition.x, this.#pupilPosition.y);
    this.#pupilShake.shake({
      duration: 400,
      magnitude: 10,
    });
  };

  #onDropComplete = () => {
    this.#allowEyeMovement = true;
    this.#keepEyeOpen = false;
    this.#closeMouth();
  };

  #createDropZone = (x, y, width, height) => {
    this.#dropZone = new Phaser.GameObjects.Zone(
      this.#scene,
      x,
      y,
      width,
      height
    );
    this.#dropZone.setRectangleDropZone(width, height);

    this.#dropZone.dragEnter = this.#dragEnter;
    this.#dropZone.dragLeave = this.#dragLeave;
    this.#dropZone.drop = this.#drop;
    this.#dropZone.onDropComplete = this.#onDropComplete;

    if (this.#debug) {
      // TESTING ONLY - draw drop zone
      const graphics = this.#scene.add.graphics();
      graphics.lineStyle(2, 0xffff00);
      graphics.strokeRect(
        this.#dropZone.x - this.#dropZone.input.hitArea.width / 2,
        this.#dropZone.y - this.#dropZone.input.hitArea.height / 2,
        this.#dropZone.input.hitArea.width,
        this.#dropZone.input.hitArea.height
      );
    }
  };
}

export default TriangleAlien;
