import Phaser from 'phaser';

import TextButton from 'src/Game/components/Buttons/TextButton';
import { displayFraction, isMobile } from 'src/lib/lib-game-utils';

class NumberLine {
  constructor(config) {
    const {
      scene,
      position,
      lineColor = 0x000000,
      arrowSize = 25,
      length = 0,
      alpha = 0.5,
      arrowGap = 40,
      markLength = 25,
      labelOffset = 50,
      denominator = 8,
      buttonsOffset = 250,
      depth = 0,
      buttonsGroupWidth = 1000,
      hintColor = 0x0000ff,
    } = config;

    this.#scene = scene;
    this.#position = position;
    this.#alpha = alpha;
    this.#arrowSize = arrowSize;
    this.#length = length;
    this.#arrowGap = arrowGap;
    this.#markLength = markLength;
    this.#lineColor = lineColor;
    this.#labelOffset = labelOffset;
    this.#denominator = denominator;
    this.#buttonsOffset = buttonsOffset;
    this.#depth = depth;
    this.#buttonsGroupWidth = buttonsGroupWidth;
    this.#hintColor = hintColor;

    this.#container = scene.add.container();
    this.#markers = scene.add.container();
    this.#buttons = scene.add.container();
    this.#dotHints = scene.add.container();

    this.#createNumberLine();

    this.#container.setDepth(depth);
    this.#dotHints.setDepth(depth);
    this.#markers.setDepth(depth);
    this.#buttons.setDepth(depth);
  }

  #scene = null;
  #position = null;
  #alpha = null;
  #length = null;
  #container = null;
  #arrowSize = null;
  #lineColor = null;
  #sections = null;
  #arrowGap = null;
  #markLength = null;
  #labelOffset = null;
  #markers = null;
  #denominator = null;
  #buttons = null;
  #buttonsOffset = null;
  #currentOption = null;
  #depth = null;
  #buttonsGroupWidth = null;
  #dotHints = null;
  #hintColor = null;

  get dotHints() {
    return this.#dotHints;
  }

  destroy = () => {
    this.#container.destroy();
    this.#container = null;

    if (this.#markers.length) {
      this.#markers.destroy();
    }
    this.#markers = null;

    if (this.#buttons.length) {
      this.#buttons.destroy();
    }
    this.#buttons = null;

    if (this.#dotHints.length) {
      this.#dotHints.destroy();
    }
    this.#dotHints = null;
  };

  showHints = (denominator, limit) => {
    this.#currentOption = denominator;
    this.#buttons.getAll().forEach((button) => button.setToggle(false));
    const button = this.#buttons.getFirst('name', denominator);
    button.setToggle(true);
    this.#drawMarkers();

    const labels = this.#getLabels();

    const length =
      typeof limit === 'number'
        ? Phaser.Math.Clamp(limit, 0, labels.length)
        : labels.length;

    for (let i = 0; i < length; i++) {
      const circle = this.#scene.add.graphics();
      circle
        .fillStyle(this.#hintColor, 0.3)
        .fillCircle(0, 0, 11)
        .setPosition(labels[i].x, labels[i].y - this.#labelOffset);

      this.#dotHints.add(circle);
    }
  };

  #createNumberLine = () => {
    const { x, y } = this.#position;

    let triangle = new Phaser.Geom.Triangle(
      x,
      y - this.#arrowSize / 2,
      x + this.#arrowSize,
      y,
      x,
      y + this.#arrowSize / 2
    );

    const rightArrowOffset = this.#length / 2 + this.#arrowGap;
    const rightArrow = this.#scene.make.graphics();
    rightArrow
      .lineStyle(1, this.#lineColor, this.#alpha)
      .strokeTriangleShape(triangle)
      .setX(rightArrowOffset);

    triangle = Phaser.Geom.Triangle.RotateAroundXY(
      triangle,
      x,
      y,
      Phaser.Math.DegToRad(180)
    );

    const leftArrowOffset = -this.#length / 2 - this.#arrowGap;
    const leftArrow = this.#scene.make.graphics();
    leftArrow
      .lineStyle(1, this.#lineColor, this.#alpha)
      .strokeTriangleShape(triangle)
      .setX(leftArrowOffset);

    const lineShape = new Phaser.Geom.Line(
      x + leftArrowOffset,
      y,
      x + rightArrowOffset,
      y
    );

    const line = this.#scene.make.graphics();
    line.lineStyle(1, this.#lineColor, this.#alpha).strokeLineShape(lineShape);
    const zeroMarker = this.#addMarker(x, y);
    const zeroMarkerLabel = this.#addLabel(x, y, '0');
    const wholeMarker = this.#addMarker(x, y, this.#length);
    const wholeMarkerLabel = this.#addLabel(x, y, '1', this.#length);

    const questionText = this.#addLabel(
      x,
      y + 70,
      'How many pieces?',
      this.#length / 2,
      isMobile(this.#scene) ? '45px' : '35px'
    );
    this.#addButtons(x - this.#length / 2, y);

    this.#container.add([
      rightArrow,
      leftArrow,
      line,
      zeroMarker,
      zeroMarkerLabel,
      wholeMarker,
      wholeMarkerLabel,
      questionText,
    ]);
  };

  #addMarker = (x, y, offset = 0, adjustlength = 0) => {
    const markerShape = new Phaser.Geom.Line(
      x,
      y - this.#markLength - adjustlength,
      x,
      y + this.#markLength + adjustlength
    );
    const marker = this.#scene.make.graphics();
    marker
      .lineStyle(1, this.#lineColor, this.#alpha)
      .strokeLineShape(markerShape)
      .setX(-this.#length / 2 + offset);

    return marker;
  };

  #addLabel = (x, y, content, offset = 0, fontSize = '25px') => {
    const text = this.#scene.make.text({
      x: x - this.#length / 2 + offset,
      y: y + this.#labelOffset,
      text: content,
      origin: 0.5,
      style: {
        fill: 'black',
        fontFamily: 'Courier',
        fontSize,
      },
    });

    return text;
  };

  #drawMarkers = () => {
    if (this.#markers.length) {
      this.#markers.removeAll(true);
    }

    const { x, y } = this.#position;

    const sectionWidth = this.#length / this.#currentOption;

    for (let i = 1; i < this.#currentOption; i++) {
      const marker = this.#addMarker(x + sectionWidth * i, y, 0, -10);

      const label = this.#addLabel(
        x + sectionWidth * i,
        y,
        displayFraction(i, this.#currentOption),
        0,
        '30px'
      );

      this.#markers.add([marker, label]);
    }
  };

  #getLabels = () =>
    this.#markers.getAll().filter((marker) => marker.type === 'Text');

  #addButtons = (x, y) => {
    if (this.#buttons.length) {
      this.#buttons.destroy();
    }

    const handleClick = (i) => {
      if (!this.#scene.started) {
        return false;
      }

      this.#buttons.getAll().forEach((button) => button.setToggle(false));
      this.#currentOption = i + 1;
      this.#drawMarkers();
      return true;
    };

    const getX = (index) => {
      const centerX = this.#scene.scale.width / 2;
      const width = isMobile(this.#scene)
        ? this.#buttonsGroupWidth + 200
        : this.#buttonsGroupWidth;
      const startingX = centerX - width / 2;

      return startingX + index * (width / this.#denominator);
    };

    for (let i = 1; i < this.#denominator; i++) {
      const button = new TextButton({
        name: i + 1,
        scene: this.#scene,
        position: {
          x: getX(i),
          y: y + this.#buttonsOffset + (isMobile(this.#scene) ? 40 : 0),
        },
        text: i + 1,
        style: {
          fill: 'black',
          fontSize: isMobile(this.#scene) ? '70px' : '45px',
          fontFamily: 'Courier',
        },
        border: {
          lineStyle: {
            lineWidth: 1,
          },
          radius: 2,
        },
        background: {
          alpha: 0.1,
          radius: 2,
          visible: false,
        },
        padding: {
          x: 10,
          y: 10,
        },
        toggleMode: true,
        depth: this.#depth,
        onClick: () => handleClick(i),
      });

      this.#buttons.add(button);
    }
  };
}

export default NumberLine;
