/**
 * @fileoverview Rectangle object - interact by cutting vertically
 */

import Phaser from 'phaser';

import RectangleSection from 'src/Game/components/RectangleSection';
import { randomInt, getScale, getNewNumber } from 'src/lib/lib-game-utils';
import ShakePosition from 'phaser3-rex-plugins/plugins/shakeposition.js';

class SliceObject {
  constructor(config) {
    const {
      scene,
      position,
      textures,
      sections = 0,
      maxWidth,
      depth = 0,
    } = config;

    this.#scene = scene;
    this.#container = scene.add.container();
    this.#position = position;
    this.#sections = sections;
    this.#textures = textures;
    this.#maxWidth = maxWidth;

    this.#createObjectGraphics();
    this.#container.setDepth(depth);
    this.#shakeContainer = new ShakePosition(this.#container);
  }

  #scene = null;
  #container = null;
  #position = null;
  #textures = null;
  #sections = null;
  #cutShape = null;
  #pointerOver = false;
  #maxWidth = null;
  #scaledWidth = null;
  #scaledHeight = null;
  #shakeContainer = null;

  get scaledWidth() {
    return this.#scaledWidth;
  }

  get scaledHeight() {
    return this.#scaledHeight;
  }

  get position() {
    return this.#position;
  }

  isPointerOver = () => this.#pointerOver;

  startFall = (index, direction) => {
    if (this.#container) {
      if (index >= 0) {
        const section = this.#container.getAll()[index];
        section.fall(direction);
      } else {
        this.#container.getAll().forEach((section) => section.fall());
      }
    }
  };

  getFallPosition = () => {
    if (this.#container?.length) {
      const section = this.#container.getFirst('falling', true);
      return section?.y;
    }
  };

  shake = () => {
    this.#shakeContainer.shake({
      mode: 'behavior',
      duration: 5000,
      magnitude: 1.25,
      magnitudeMode: 'decay',
    });
  };

  stopShake = () => {
    this.#shakeContainer.stop();
  };

  destroy = () => {
    this.#clear();
    this.#container = null;
  };

  #clear = () => {
    this.#container.destroy(true);
  };

  #createObjectGraphics = () => {
    const texture = this.#getRandomTexture();

    const objectSections = [];
    for (let i = 0; i < this.#sections; i++) {
      const section = new RectangleSection({
        scene: this.#scene,
        position: this.#position,
        texture,
        add: true,
      });
      objectSections.push(section);
    }

    this.#container.add(objectSections);

    const firstSection = this.#container.getFirst('visible', true);

    const fullWidth = firstSection.width;
    const fullHeight = firstSection.height;

    const scaleRatio = this.#maxWidth
      ? getScale(fullWidth, this.#maxWidth) // scale factor for max width
      : 1;

    this.#scaledWidth = fullWidth * scaleRatio;
    this.#scaledHeight = fullHeight * scaleRatio;

    this.#container.getAll().forEach((section, index) => {
      const sectionWidth = section.width / this.#sections;
      const graphicX = index * sectionWidth;
      const translateX = section.x + index * (sectionWidth * scaleRatio);
      const originX = index / this.#sections;
      const translateOrigX = 1 / this.#sections / 2;

      section
        .setSize(sectionWidth, section.height)
        .setCrop(graphicX, 0, sectionWidth, section.height) // crop each section
        .setOrigin(originX + translateOrigX, 0.5) // center each section's rotational pivot point
        .setX(translateX + (sectionWidth * scaleRatio) / 2) // space out the slice sections
        .setScale(scaleRatio); // scales the image to maxWidth
    });

    const translateContainerX = -(firstSection.width * scaleRatio) / 2;

    this.#container.setX(translateContainerX); // translate x of container to center image

    const cutShape = new Phaser.Geom.Rectangle(
      this.#position.x,
      this.#position.y - (fullHeight * scaleRatio) / 2,
      fullWidth * scaleRatio,
      fullHeight * scaleRatio
    );

    this.#container
      .setInteractive(cutShape, Phaser.Geom.Rectangle.Contains)
      .on('pointerover', () => (this.#pointerOver = true))
      .on('pointerout', () => (this.#pointerOver = false));
  };

  #getRandomTexture = (nextTextureCanBeSame = false) => {
    if (this.#textures.length === 1) {
      this.#scene.textureIndex = 0;
      return this.#textures[0];
    }

    let roll;

    if (!nextTextureCanBeSame) {
      roll = getNewNumber(this.#scene.textureIndex, () =>
        randomInt(0, this.#textures.length)
      );
    } else {
      roll = randomInt(0, this.#textures.length);
    }

    this.#scene.textureIndex = roll;

    return this.#textures[roll];
  };
}

export default SliceObject;
