import Phaser from 'phaser';

const RADIUS = 60;
const OFFSHOOT = 0.02;
const BACKGROUND_COLOR = 0x2f4f4f;
const COLOR = 0x05eb23;
const THICKNESS = 15;
const ALPHA = 0.8;
const MAX_POINTS = 100;
const RADIAL_OFFSET = 0;

class RadialProgressBar {
  constructor(config) {
    const {
      scene,
      position,
      radius = RADIUS,
      offshoot = OFFSHOOT,
      backgroundColor = BACKGROUND_COLOR,
      color = COLOR,
      thickness = THICKNESS,
      alpha = ALPHA,
      maxPoints = MAX_POINTS,
      radialOffset = RADIAL_OFFSET,
    } = config;

    this.#scene = scene;
    this.#position = position;
    this.#radius = radius;
    this.#offshoot = offshoot;
    this.#radialBackgroundColor = backgroundColor;
    this.#color = color;
    this.#thickness = thickness;
    this.#alpha = alpha;
    this.#maxPoints = maxPoints;
    this.#radialOffset = radialOffset;

    this.#container = scene.add.container();

    this.#addGraphics();
  }

  #scene = null;
  #container = null;
  #position = null;
  #radius = null;
  #offshoot = null;
  #angleTween = 0;
  #initTween = null;
  #radialBackground = null;
  #radialBackgroundColor = null;
  #color = null;
  #radialForeground = null;
  #angleMin = 0;
  #angleMax = 0;
  #thickness = null;
  #alpha = null;
  #maxPoints = null;
  #radialOffset = null;

  destory = () => {
    this.#container.destroy(true);
    this.#container = null;

    if (this.#initTween) {
      this.#initTween.remove();
      this.#initTween = null;
    }

    if (this.#angleTween) {
      this.#angleTween.remove();
      this.#angleTween = null;
    }
  };

  draw = () => {
    if (!this.#radialForeground || !this.#angleTween) {
      return;
    }

    const angle = this.#angleTween.getValue();

    this.#radialForeground.clear();

    this.#radialForeground.lineStyle(this.#thickness - 10, this.#color);
    this.#radialForeground.beginPath();
    this.#radialForeground.arc(
      this.#position.x,
      this.#position.y,
      this.#radius,
      Phaser.Math.DegToRad(0 + this.#radialOffset),
      Phaser.Math.DegToRad(angle + this.#radialOffset),
      false,
      this.#offshoot
    );
    this.#radialForeground.strokePath();
    this.#radialForeground.closePath();
  };

  addProgress = (points, callback = () => {}) => {
    if (!points) {
      callback();
      return;
    }

    if (this.#angleMax === 0) {
      this.#radialForeground.setVisible(true);
      this.#radialBackground.setVisible(true);

      this.#initTween = this.#scene.tweens.add({
        targets: this.#container.getAll(),
        alpha: { from: 0, to: this.#alpha },
        ease: 'Ease',
        duration: 500,
        onComplete: () => this.#initTransition(points, callback),
      });
    } else {
      this.#initTransition(points, callback);
    }
  };

  #initTransition = (points, callback = () => {}) => {
    const point = 360 / this.#maxPoints;
    const pointsTotal = point * points;
    this.#angleMin = this.#angleMax;
    this.#angleMax += pointsTotal;

    this.#angleTween = this.#scene.tweens.addCounter({
      from: this.#angleMin,
      to: this.#angleMax,
      ease: 'Power2',
      duration: 6000 * (points / this.#maxPoints),
      onComplete: () => {
        this.#angleTween.remove();
        this.#angleTween = null;
        callback();
      },
    });
  };

  #addGraphics = () => {
    // background
    this.#radialBackground = this.#scene.add.graphics();
    this.#radialBackground.lineStyle(
      this.#thickness,
      this.#radialBackgroundColor,
      this.#alpha
    );
    this.#radialBackground.beginPath();
    this.#radialBackground.arc(
      this.#position.x,
      this.#position.y,
      this.#radius,
      Phaser.Math.DegToRad(0 + this.#radialOffset),
      Phaser.Math.DegToRad(360 + this.#radialOffset),
      true,
      this.#offshoot
    );
    this.#radialBackground.strokePath();
    this.#radialBackground.closePath();
    this.#radialBackground.setVisible(false);

    // foreground
    this.#radialForeground = this.#scene.add.graphics();
    this.#radialForeground.setVisible(false);

    this.#container.add([this.#radialBackground, this.#radialForeground]);
  };
}

export default RadialProgressBar;
