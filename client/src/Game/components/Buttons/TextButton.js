import Phaser from 'phaser';

import RexButton from 'phaser3-rex-plugins/plugins/button.js';

class TextButton extends Phaser.GameObjects.Text {
  constructor(config) {
    const {
      scene,
      name = '',
      position,
      alpha = 1,
      text = 'button',
      style,
      padding,
      border = null,
      background = null,
      circle = null,
      depth = 1,
      onClick = () => null,
      toggleMode = false,
      disable = false,
    } = config;
    const { x, y } = position;

    super(scene, x, y, text);

    this.setOrigin(0.5, 1)
      .setAlpha(alpha)
      .setStyle(style)
      .setDepth(depth)
      .setName(name);

    this.#container = scene.add.container();

    this.#disable = disable;

    if (toggleMode) {
      this.#toggle = false;
    }

    if (padding) {
      if (padding.x && padding.y) {
        this.setPadding(padding.x, padding.y);
      }
    }

    this.#position = {
      x: x - this.width / 2, // offset for text origin
      y: y - this.height, // offset for text origin
    };

    if (border) {
      this.#border = scene.add.graphics();

      if (border.background) {
        const {
          backgroundColor = 0x000000,
          alpha: backgroundAlpha = alpha,
          radius: backgroundRadius = 0,
        } = border.background;

        this.#border
          .fillStyle(backgroundColor, backgroundAlpha)
          .fillRoundedRect(
            this.#position.x,
            this.#position.y,
            this.width,
            this.height,
            backgroundRadius
          );
      }

      const { lineStyle, radius: borderRadius = 0, visible = true } = border;

      if (lineStyle) {
        const {
          lineWidth = 1,
          color = 0x000000,
          alpha: borderAlpha = alpha,
        } = lineStyle;

        this.#border
          .lineStyle(lineWidth, color, borderAlpha)
          .strokeRoundedRect(
            this.#position.x,
            this.#position.y,
            this.width,
            this.height,
            borderRadius
          );
      }

      this.#border.setDepth(depth).setVisible(visible);
      this.#container.add(this.#border);
    }

    if (background) {
      this.#background = scene.add.graphics();

      const {
        backgroundColor = 0x000000,
        alpha: backgroundAlpha = alpha,
        radius: backgroundRadius = 0,
        visible = true,
      } = background;

      this.#background
        .fillStyle(backgroundColor, backgroundAlpha)
        .fillRoundedRect(
          this.#position.x,
          this.#position.y,
          this.width,
          this.height,
          backgroundRadius
        );

      this.#background.setDepth(depth).setVisible(visible);
      this.#container.add(this.#background);
    }

    if (circle) {
      this.#circle = scene.add.graphics();

      const {
        color: circleColor = 0x000000,
        alpha: circleAlpha = alpha,
        radius: circleRadius = 1,
        offsetX: circleOffsetX = 0,
        offsetY: circleOffsetY = 0,
        visible = true,
      } = circle;

      this.#circle
        .fillStyle(circleColor, circleAlpha)
        .fillCircle(
          this.#position.x + circleOffsetX,
          this.#position.y + circleOffsetY,
          circleRadius
        );

      this.#circle.setDepth(depth).setVisible(visible);
      this.#container.add(this.#circle);
    }

    this.#container.add(this).setDepth(depth);

    this.#createButtonFunctionality(onClick);

    // set button cursor
    this.setInteractive()
      .on('pointerover', () => (this.#hover = true))
      .on('pointerout', () => (this.#hover = false));
    this.input.cursor = 'pointer';

    this.on('destroy', this.destroy);
  }

  #container = null;
  #position = null;
  #button = null;
  #border = null;
  #background = null;
  #circle = null;
  #toggle = null;
  #disable = null;
  #hover = false;

  get hover() {
    return this.#hover;
  }

  getButton = () => {
    return this.#container;
  };

  setBackgroundColor = (color, alpha = 1) => {
    if (!color) {
      this.#background.setVisible(false);
      return;
    }

    this.#background
      .setVisible(true)
      .fillStyle(color, alpha)
      .fillRoundedRect(
        this.#position.x,
        this.#position.y,
        this.width,
        this.height
      );
  };

  destroy = () => {
    this.#button.destroy();
    this.#button = null;

    if (this.#container) {
      this.#container.destroy();
      this.#container = null;
    }
  };

  setBorderVisible = (visible) => {
    this.#border.setVisible(visible);
  };

  setToggle = (toggle) => {
    this.#toggle = toggle;
    this.#background.setVisible(this.#toggle);
  };

  #createButtonFunctionality = (onClick) => {
    this.#button = new RexButton(this, {
      mode: 'release',
      clickInterval: 100,
    });

    this.#button.on('click', () => {
      if (this.#disable) {
        return;
      }

      const clicked = onClick();
      const isButtonClicked = clicked === true || clicked === undefined;

      if (
        typeof this.#toggle === 'boolean' &&
        this.#background &&
        isButtonClicked
      ) {
        this.setToggle(!this.#toggle);
      }
    });
  };
}

export default TextButton;
