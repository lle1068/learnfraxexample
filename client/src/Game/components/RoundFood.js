import Phaser from 'phaser';

import Slice from 'src/Game/components/TriangleSection';

class RoundFood {
  constructor(config) {
    const {
      scene,
      texture,
      position,
      maxWidth = 500,
      slices = 6,
      depth = 0,
      onGrab = () => {},
      onDrop = () => {},
      introAnimation,
      guideLineThickness,
      denominator = slices,
    } = config;

    this.#scene = scene;
    this.#container = scene.add.container();
    this.#slices = slices;
    this.#denominator = denominator;

    this.#border = scene.add.graphics();

    const { x, y } = position;
    const length = maxWidth + 40;
    const offset = length / 2;

    this.#border
      .lineStyle(2, 0x0000ff)
      .strokeRoundedRect(x - offset, y - offset, length, length, 5)
      .setVisible(false);

    for (let i = 0; i < slices; i++) {
      const slice = new Slice({
        scene,
        texture,
        position,
        maxWidth,
        angle: i * Phaser.Math.DegToRad(360 / slices),
        angleOffset: -Phaser.Math.DegToRad(360 / slices) / 2, // offset angle so it's easier to see the fractions
        totalSections: slices,
        depth,
        onGrab,
        onDrop,
        introAnimation,
        delay: i * 50,
        guideLineThickness,
        denominator,
      });

      this.#container.add(slice);
    }

    this.#container.setDepth(depth);
  }

  get denominator() {
    return this.#denominator;
  }

  get eaten() {
    return this.#slices - this.#container.length;
  }

  destroy = () => {
    this.#container.destroy();
    this.#container = null;
    this.#border.destroy();
    this.#border = null;
  };

  showHighlight = () => {
    this.#border.setVisible(true);

    this.#scene.tweens.add({
      targets: this.#border,
      alpha: { from: 0, to: 1 },
      duration: 500,
      ease: 'Linear',
      yoyo: true,
      loop: -1,
    });
  };

  #scene = null;
  #container = null;
  #slices = null;
  #denominator = null;
  #border = null;
}

export default RoundFood;
