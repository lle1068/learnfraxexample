import Phaser from 'phaser';

class Hint extends Phaser.GameObjects.Text {
  constructor(config) {
    const {
      scene,
      position = { x: 0, y: 0 },
      alpha = 1,
      text = 'hint',
      style,
      depth = 1,
      mode = 'blink',
      show = false,
      duration = 800,
    } = config;
    const { x, y } = position;

    super(scene, x, y, text);

    this.scene.add.existing(this);

    this.#duration = duration;

    this.setOrigin(0.5)
      .setAlpha(alpha)
      .setStyle(style)
      .setDepth(depth)
      .setVisible(false);

    const activate = {
      blink: this.#activateBlink,
    };

    this.#activate = activate[mode];

    if (show) {
      this.show();
    }
  }

  #blink = null;
  #end = false;
  #activate = null;
  #duration = null;

  show = () => {
    this.#activate();
  };

  hide = () => {
    this.#end = true;
  };

  #activateBlink = () => {
    this.#end = false;
    this.setVisible(true);

    this.#blink = this.scene.tweens.add({
      targets: this,
      alpha: { from: 0, to: 1 },
      duration: this.#duration,
      ease: 'Linear',
      yoyo: true,
      onLoop: () => {
        if (this.#end) {
          this.#blink.stop();
          this.setVisible(false);
        }
      },
      loop: -1,
    });
  };
}

export default Hint;
