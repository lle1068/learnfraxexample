import Phaser from 'phaser';

import { getScale } from 'src/lib/lib-game-utils';

// manually adjust base triangle size for number of sections
const BASE_MULTIPLIER_MAP = Object.freeze({
  3: 1.65,
  4: 1.27,
  5: 1.15,
  6: 1.1,
  7: 1.07,
  8: 1.05,
  9: 1.04,
  10: 1.03,
  11: 1.02,
  12: 1.01,
});

class TriangleSection extends Phaser.GameObjects.Image {
  constructor(config) {
    const {
      scene,
      texture,
      position,
      maxWidth,
      add = false,
      angle = 0,
      angleOffset = 0,
      totalSections,
      depth = 0,
      drawOutline = true,
      onGrab = () => {},
      onDrop = () => {},
      introAnimation = false,
      delay = 0,
      returnToPosition = true,
      guideLineThickness = 3,
      denominator = totalSections,
    } = config;

    const { x, y } = position;

    super(scene, x, y, texture);

    if (add) {
      scene.add.existing(this);
    }

    this.#position = position;
    this.#returnToPosition = returnToPosition;
    this.#denominator = denominator;

    const scaleRatio = getScale(this.width, maxWidth);
    const radius = this.width / 2;

    this.#shape = scene.add.graphics();
    this.#shape
      .setPosition(x, y)
      .fillStyle(0xffffff, 0)
      .setScale(scaleRatio)
      .setDepth(depth);

    if (totalSections === 2) {
      const isSecond = Phaser.Math.RadToDeg(angle) === 180;
      const rect = new Phaser.Geom.Rectangle(
        isSecond ? 0 : -radius,
        -radius,
        radius,
        this.height
      );

      this.#shape.fillRectShape(rect);

      if (drawOutline) {
        this.#createOutline(x, y, radius, scaleRatio, guideLineThickness);
        this.#shape.strokeRectShape(rect);
      }

      this.setInteractive(
        Phaser.Geom.Rectangle.Offset(rect, radius, radius),
        Phaser.Geom.Rectangle.Contains
      )
        .on('pointerdown', (pointer) =>
          this.#handlePointerDown(pointer, onGrab)
        )
        .on('pointerup', () => this.#handlePointerUp(onDrop));
    } else {
      // find the base length of the triangle using a circle's circumference divided by number of total sections
      const circleShape = new Phaser.Geom.Circle(x, y, radius);
      const circumference = Phaser.Geom.Circle.Circumference(circleShape);

      const base =
        (circumference / totalSections) *
        (BASE_MULTIPLIER_MAP[totalSections] ?? 1);

      // build triangle
      let triangle = new Phaser.Geom.Triangle(
        0,
        0,
        base / 2,
        radius,
        -base / 2,
        radius
      );

      triangle = Phaser.Geom.Triangle.RotateAroundXY(
        triangle,
        0,
        0,
        angleOffset
      );
      triangle = Phaser.Geom.Triangle.RotateAroundXY(triangle, 0, 0, angle);

      this.#shape.fillTriangleShape(triangle);

      if (drawOutline) {
        this.#createOutline(x, y, radius, scaleRatio, guideLineThickness);
        this.#shape.strokeTriangleShape(triangle);
      }

      this.setInteractive(
        Phaser.Geom.Triangle.Offset(triangle, radius, radius),
        Phaser.Geom.Triangle.Contains
      )
        .on('pointerdown', (pointer) =>
          this.#handlePointerDown(pointer, onGrab)
        )
        .on('pointerup', () => this.#handlePointerUp(onDrop));
    }

    const mask = this.#shape.createGeometryMask();
    this.setMask(mask).setScale(scaleRatio);

    this.on('destroy', () => {
      this.clearMask(true);
      this.#shape.clearMask(true);
      this.#shape.destroy();
      this.#shape = null;

      if (this.#circleShape) {
        this.#circleShape.clearMask(true);
        this.#circleShape.destroy();
        this.#circleShape = null;
      }
    });

    scene.input.setDraggable(this);

    if (introAnimation) {
      this.#shape.setAlpha(0);
      this.setAlpha(0);

      const intro = scene.tweens.add({
        targets: [this, this.#shape],
        alpha: { from: 0, to: 1 },
        duration: 500,
        ease: 'Linear',
        delay,
        onComplete: () => {
          intro.remove();
          if (this) {
            this.setAlpha(1);
            this.#shape?.setAlpha(1);
          }
        },
      });
    }
  }

  #shape = null;
  #circleShape = null;
  #position = null;
  #eaten = false;
  #returnToPosition = null;
  #denominator = null;

  get denominator() {
    return this.#denominator;
  }

  shrinkFade = (duration, position, onComplete = () => {}) => {
    this.#eaten = true;
    this.drag(position.x, position.y);

    const shrinkTween = this.scene.tweens.add({
      targets: this,
      alpha: { from: 1, to: 0 },
      scale: { from: this.scale, to: 0 },
      duration,
      ease: 'Power2',
      onComplete: () => {
        shrinkTween.remove();
        onComplete();
      },
    });
  };

  drag = (x, y) => {
    this.setPosition(x, y);
    this.#shape.setPosition(x, y);

    if (!this.#returnToPosition && this.#circleShape) {
      this.#circleShape.destroy();
      this.#circleShape = null;
    }
  };

  #createOutline = (x, y, radius, scaleRatio, guideLineThickness) => {
    const circleForMask = new Phaser.Geom.Circle(
      0,
      0,
      radius * scaleRatio * 0.95
    );

    // add mask on the outline to prevent showing the ends of the triangle
    this.#circleShape = this.scene.make.graphics();
    this.#circleShape.fillCircleShape(circleForMask).setPosition(x, y);
    const circleMask = this.#circleShape.createGeometryMask();

    // draw outline for better slice visibility
    this.#shape.setMask(circleMask).lineStyle(guideLineThickness, 0xffffff, 1);
  };

  #handlePointerDown = (pointer, callback) => {
    this.scene.input.setDefaultCursor('grabbing');
    this.#shape.setVisible(false);
    callback();
  };

  #handlePointerUp = (callback) => {
    this.scene.input.setDefaultCursor('grab');
    callback();

    if (this.#returnToPosition && !this.#eaten) {
      this.setPosition(this.#position.x, this.#position.y);
      this.#shape.setPosition(this.x, this.y).setVisible(true);
    }
  };
}

export default TriangleSection;
