/**
 * @fileoverview Rectangle sections that have falling physics
 */

import Phaser from 'phaser';
import ShakePosition from 'phaser3-rex-plugins/plugins/shakeposition.js';

import { randomInt } from 'src/lib/lib-game-utils';

class RectangleSection extends Phaser.Physics.Arcade.Image {
  constructor(config) {
    const { scene, position, texture, add = false } = config;
    const { x, y } = position;

    super(scene, x, y, texture);

    if (add) {
      scene.physics.add.existing(this);
    }

    this.#shake = new ShakePosition(this);
  }

  #shake = null;
  #falling = false;

  get falling() {
    return this.#falling;
  }

  fall = (direction) => {
    this.#falling = true;
    this.setCollideWorldBounds(false);
    this.setGravityY(600);

    switch (direction) {
      case 'left': {
        this.setVelocityX(-randomInt(150, 251));
        this.setAngularVelocity(-randomInt(25, 251));
        break;
      }
      case 'right': {
        this.setVelocityX(randomInt(150, 251));
        this.setAngularVelocity(randomInt(25, 251));
        break;
      }
      default: {
        this.setVelocityX(randomInt(150, 251, true));
        this.setAngularVelocity(randomInt(25, 251, true));
      }
    }
  };

  shake = () => {
    this.#shake.shake({
      duration: 500,
      magnitude: 1,
    });
  };

  stopShake = () => this.#shake.stop();
}

export default RectangleSection;
