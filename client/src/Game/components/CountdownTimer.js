import Phaser from 'phaser';

class CountdownTimer extends Phaser.GameObjects.Text {
  constructor(config) {
    const {
      scene,
      position: { x, y },
      style,
      seconds,
      add = false,
      onComplete = () => null,
    } = config;

    super(scene, x, y, seconds, style);

    this.setOrigin(0.5, 0);

    const second = 1000;

    this.#initialPosition = { x, y };
    this.#onComplete = onComplete;
    this.#seconds = seconds;
    this.#timer = scene.time.addEvent({
      delay: second,
      startAt: second,
      callback: this.#nextCount,
      repeat: seconds,
      timeScale: 1,
    });
    this.#timerTween = scene.tweens.add({
      targets: this,
      alpha: { from: 0, to: 1 },
      duration: second / 2,
      ease: 'Back',
      repeat: seconds,
      yoyo: true,
      onComplete: () => {
        this.#timerTween.remove();
        this.#timerTween = null;
      },
    });

    if (add) {
      scene.add.existing(this);
    }
  }

  #initialPosition = null;
  #timer = null;
  #timerTween = null;
  #seconds = null;
  #onComplete = null;
  #completed = false;

  get completed() {
    return this.#completed;
  }

  pause = () => {
    this.#timer.paused = true;
    if (this.#timerTween) {
      this.#timerTween.pause();
    }
  };

  resume = () => {
    this.#timer.paused = false;
    if (this.#timerTween) {
      if (this.#timerTween.hasStarted) {
        this.#timerTween.resume();
      } else {
        this.#timerTween.play();
      }
    }
  };

  #nextCount = () => {
    if (this.#seconds > 0) {
      this.setText(this.#seconds);
    } else {
      this.#onComplete();
      this.setVisible(false);
      this.#completed = true;
    }
    this.#seconds -= 1;
  };
}

export default CountdownTimer;
