/**
 * @fileoverview draws loading bar for preload phase
 */

import { resolution } from 'src/Game/config';

const drawLoadingBar = (config) => {
  const {
    scene,
    width = 600,
    height = 60,
    backgroundColor = 0x212121,
    foregroundColor = 0x4caf50,
    textColor = 'white',
    fontFamily = 'Roboto',
    thickness = 5,
    radius = 5,
  } = config;

  const progressBar = scene.add.graphics();
  const progressBox = scene.add.graphics();

  progressBar.setDepth(2);
  progressBox.setDepth(1);

  const x = scene.cameras.main.width / 2;
  const y = scene.cameras.main.height / 2;

  const loadingText = scene.make.text({
    x,
    y: y - 100,
    text: 'Loading...',
    style: {
      fontFamily,
      fontSize: '50px',
      fill: textColor,
    },
    depth: 3,
  });

  const percentText = scene.make.text({
    x,
    y,
    text: '0%',
    style: {
      fontFamily,
      fontSize: '25px',
      fill: textColor,
    },
    depth: 3,
  });

  const assetText = scene.make.text({
    x,
    y: y + 100,
    text: '',
    style: {
      fontFamily,
      fontSize: '40px',
      fill: textColor,
    },
    depth: 3,
  });

  const rectX = resolution.width / 2 - width / 2 - thickness;
  const rectY = y - height / 2;

  scene.load.once('start', () => {
    progressBox.fillStyle(backgroundColor, 1);
    progressBox.fillRoundedRect(rectX, rectY, width, height, radius);
    loadingText.setOrigin(0.5);
    percentText.setOrigin(0.5);
    assetText.setOrigin(0.5);
  });

  scene.load.on('progress', (progress) => {
    progressBar.clear();
    progressBar.fillStyle(foregroundColor, 1);
    progressBar.fillRect(
      rectX,
      rectY + thickness,
      (width - 10) * progress,
      height - 10
    );
    percentText.setText(parseInt(progress * 100) + '%');
  });

  scene.load.on('fileprogress', (file) => {
    assetText.setText('Asset: ' + file.key);
  });

  const cleanup = () => {
    progressBar.destroy();
    progressBox.destroy();
    loadingText.destroy();
    percentText.destroy();
    assetText.destroy();
  };

  scene.load.once('loaderror', (err) => {
    cleanup();

    console.log('Error: ', err);
    const errorText = scene.make.text({
      x,
      y,
      text: `Error! Please check console for details.`,
      style: {
        fontFamily,
        fontSize: '50px',
        fill: textColor,
      },
    });
    errorText.setOrigin(0.5);
  });

  scene.load.once('complete', cleanup);
};

export default drawLoadingBar;
