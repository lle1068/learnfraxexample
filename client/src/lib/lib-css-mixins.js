/**
 * @fileoverview css mixins
 */

import { css } from 'styled-components';

/**
 * @description standard container fills the view
 */
export const container = css`
  min-height: 100vh;
`;

/**
 * @description adjust height size to account for a header
 */
export const containerWithHeader = css`
  ${(props) => `height: calc(100vh - ${props.theme.config.headerHeight})`};
`;

/**
 * @description add to style NavLink with disabled prop
 */
export const disabledLink = css`
  ${(props) =>
    props.disabled &&
    `
      color: ${props.theme.colors.disabled};
      &:hover {
        color: ${props.theme.colors.disabled};
        cursor: default;
        text-decoration: none;
      }
    `};
`;
