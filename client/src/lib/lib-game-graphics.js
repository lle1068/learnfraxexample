/**
 * @fileoverview game graphical utility methods
 */

import drawLoadingBar from 'src/Game/graphics/drawLoadingBar';

export { drawLoadingBar };
