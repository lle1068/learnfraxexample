/**
 * @fileoverview game utility methods
 */

import Phaser from 'phaser';
import { Pinch } from 'phaser3-rex-plugins/plugins/gestures.js';

import { resolution } from 'src/Game/config';

export const offsetFromAnchor = (anchor, offsetPosition) => {
  const { x, y } = offsetPosition;
  return { x: anchor.x + x, y: anchor.y + y };
};

// excludes max
export const randomNumber = (min, max) => Math.random() * (max - min) + min;

export const coinFlip = () => Math.random() >= 0.5;

// excludes max
export const randomInt = (min, max, flipSign = false) => {
  const value = Math.floor(randomNumber(min, max));
  return flipSign && coinFlip() ? -value : value;
};

export const randomEvenInt = (min, max) =>
  Math.floor(randomInt(min, max) / 2) * 2;

export const getScale = (width, maxWidth) => maxWidth / width;

// e.g. pupil within eye movement
export const drawBoundCircularMovement = (
  input,
  boundingObject,
  movingObject,
  radius
) => {
  const dx = boundingObject.x - input.activePointer.worldX;
  const dy = boundingObject.y - input.activePointer.worldY;
  const r = Math.sqrt(dx * dx + dy * dy);
  const x = r < radius ? dx : (dx * radius) / r;
  const y = r < radius ? dy : (dy * radius) / r;
  movingObject.setPosition(
    boundingObject.x + x * -1,
    boundingObject.y + y * -1
  );
};

export const getRandomDivider = (max, currentDivider) => {
  let roll = null;

  do {
    roll = randomInt(2, max + 1);
  } while (roll === currentDivider);

  return roll;
};

export const hexToDec = (hexString) => parseInt(hexString, 16);

export const convertHexColorToRGB = (hex) => {
  let index = 0;
  let max = hex.length;

  if (hex.startsWith('#')) {
    ++index;
    --max;
  }

  const array = [];

  for (index; index < max; index += 2) {
    const code = hex.substring(index, index + 2);
    array.push(hexToDec(code));
  }

  return array;
};

export const getNewNumber = (current, method) => {
  let value = null;

  do {
    value = method();
  } while (value === current);

  return value;
};

export const roundPrecision = (value, precision) => {
  const multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
};

export const initPinchZoom = (scene, threshold = 10, panLimitFactor = 4) => {
  const pinch = new Pinch(scene, { threshold });
  const camera = scene.cameras.main;

  pinch
    .on(
      'drag1',
      (touch) => {
        if (!scene.started || !scene.pinchZoom) {
          return;
        }

        const drag1Vector = touch.drag1Vector;
        const scrollX = Math.round(
          camera.scrollX - drag1Vector.x / camera.zoom
        );
        const scrollY = Math.round(
          camera.scrollY - drag1Vector.y / camera.zoom
        );

        const x = Phaser.Math.Clamp(
          scrollX,
          -resolution.width / panLimitFactor,
          resolution.width / panLimitFactor
        );
        const y = Phaser.Math.Clamp(
          scrollY,
          -resolution.height / panLimitFactor,
          resolution.height / panLimitFactor
        );

        camera.setScroll(x, y);
      },
      scene
    )
    .on(
      'pinch',
      (pinch) => {
        if (!scene.started || !scene.pinchZoom) {
          return;
        }

        const scaleFactor = pinch.scaleFactor;
        let zoomFactor = camera.zoom * scaleFactor;

        if (zoomFactor < 1) {
          zoomFactor = 1;
        }

        camera.setZoom(roundPrecision(zoomFactor, 2));
      },
      scene
    );

  return pinch;
};

export const isMobile = (scene) => {
  const { android, iOS, iPhone } = scene.sys.game.device.os;
  return android || iOS || iPhone;
};

export const displayFraction = (numerator, denominator) => {
  const superscript = {
    0: '⁰',
    1: '¹',
    2: '²',
    3: '³',
    4: '⁴',
    5: '⁵',
    6: '⁶',
    7: '⁷',
    8: '⁸',
    9: '⁹',
  };

  const subscript = {
    0: '₀',
    1: '₁',
    2: '₂',
    3: '₃',
    4: '₄',
    5: '₅',
    6: '₆',
    7: '₇',
    8: '₈',
    9: '₉',
  };

  const unicodeMap = {
    '1/4': '\u{00BC}',
    '1/2': '\u{00BD}',
    '3/4': '\u{00BE}',
    '1/7': '\u{2150}',
    '1/9': '\u{2151}',
    '1/10': '\u{2152}',
    '1/3': '\u{2153}',
    '2/3': '\u{2154}',
    '1/5': '\u{2155}',
    '2/5': '\u{2156}',
    '3/5': '\u{2157}',
    '4/5': '\u{2158}',
    '1/6': '\u{2159}',
    '5/6': '\u{215A}',
    '1/8': '\u{215B}',
    '3/8': '\u{215C}',
    '5/8': '\u{215D}',
    '7/8': '\u{215E}',
    '1/': '\u{215F}',
  };

  const slash = '\u{2044}';

  const strFraction = `${numerator}/${denominator}`;

  if (unicodeMap[strFraction]) {
    return unicodeMap[strFraction];
  }

  return `${superscript[numerator]}${slash}${subscript[denominator]}`;
};

export const isPositionWithinSelectionBox = (pos, rect) => {
  const isPositiveWidth = Math.sign(rect.width) === 1;
  const isPositiveHeight = Math.sign(rect.height) === 1;

  const leftBound = isPositiveWidth ? rect.x : rect.x + rect.width;
  const rightBound = isPositiveWidth ? rect.x + rect.width : rect.x;
  const topBound = isPositiveHeight ? rect.y : rect.y + rect.height;
  const bottomBound = isPositiveHeight ? rect.y + rect.height : rect.y;

  if (
    pos.x >= leftBound &&
    pos.x <= rightBound &&
    pos.y >= topBound &&
    pos.y <= bottomBound
  ) {
    return true;
  }

  return false;
};

export const verifyType = (type, variable) => {
  if (typeof variable !== type) {
    console.warn(`Error: ${variable} needs to be of type ${type}.`);
    return false;
  }

  return true;
};
