/**
 * @fileOverview this file provides custom renders and utlity methods for testing React components
 */

import React from 'react';
import { ThemeProvider } from 'styled-components';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createMemoryHistory } from 'history';

import globalTheme from 'src/components/Styled/globalTheme';
import reducers from 'src/redux/reducers';

// this is a handy function that I normally make available for all my tests
// that deal with connected components.
// you can provide initialState for the entire store that the ui is rendered with
function customRender(
  ui,
  {
    initialState,
    store = createStore(reducers, initialState, applyMiddleware(thunk)),
  } = {},
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) {
  const Wrapper = ({ children }) => (
    <Provider store={store}>
      <ThemeProvider theme={globalTheme}>
        <Router history={history}>{children}</Router>
      </ThemeProvider>
    </Provider>
  );
  return {
    ...render(ui, { wrapper: Wrapper }),
    // adding `store` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    store,
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history,
  };
}

export * from '@testing-library/react';

export { customRender as render, userEvent };
