/**
 * @fileoverview server entry point
 */

const LOCAL_PORT = 3000;

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(express.json());
app.use(bodyParser.json()); // middleware to assign req.body property

// https://webdva.github.io/how-to-force-express-https-tutorial/
// app.use((req, res, next) => {
//   if (process.env.NODE_ENV === 'production') {
//     if (req.headers['x-forwarded-proto'] !== 'https')
//       // the statement for performing our redirection
//       return res.redirect('https://' + req.headers.host + req.url);
//     else return next();
//   } else return next();
// });

if (process.env.NODE_ENV === 'production') {
  // express will serve up our production assets like main.js file or main.css
  app.use(express.static('client/build'));
  // express will serve index.html if it doesn't recognize the route
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const PORT =
  process.env.NODE_ENV === 'test' ? 0 : process.env.PORT || LOCAL_PORT;
// use port 0 to take advantage of running tests in parallel
const server = app.listen(PORT, () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Server listening on port ${PORT}...`);
  }
});

// to allow jest to close server after tests
app.close = async () => {
  server.close();
};

module.exports = app;
