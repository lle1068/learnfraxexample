#!/usr/bin/env bash

# exit when any command fails
set -e

npm run lint:strict
# npm run test # uncomment after adding in back end tests
cd client
npm run lint:strict
npm run test
cd ..

BRANCH=$(git branch --show-current)

git push origin $BRANCH
